/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use std::{
    env,
    path::PathBuf,
    thread,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

use crate::repl::{self, test_repl};

#[test]
fn test_exit() {
    let (interface, repl_thread) = setup_test();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_config() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("config-get input").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!(kconfig, line);
    interface.writeln("config-get output").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!(dotconfig, line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_list() {
    let (interface, repl_thread) = setup_test();
    interface.writeln("config-get list").unwrap();
    let line1 = interface.readln().unwrap();
    let line2 = interface.readln().unwrap();
    let line3 = interface.readln().unwrap();
    assert_ne!("", line1);
    assert_ne!("", line2);
    assert_ne!("", line3);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[ ] BOOL: n", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_info_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_info_function";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{}", line);
    assert_eq!("Hello World!", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_info_not_enough_params_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_info_function_not_enough_params";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains("Macro function 'info' expects 1 argument, 0 given"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_info_too_many_params_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_info_function_too_many_params";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains("Macro function 'info' expects 1 argument, 2 given"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_warning_if_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_warning_if_function";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.starts_with(kconfig));
    assert!(line.ends_with("Warning"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_warning_if_function_not_enough_params() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_warning_if_function_not_enough_params";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains("Macro function 'warning-if' expects 2 arguments, 1 given"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_warning_if_function_too_many_params() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_warning_if_function_too_many_params";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains("Macro function 'warning-if' expects 2 arguments, 3 given"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_error_if_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_error_if_function";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains(kconfig));
    assert!(line.ends_with("Error"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_error_if_function_not_enough_params() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_error_if_function_not_enough_params";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains("Macro function 'error-if' expects 2 arguments, 1 given"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_error_if_function_too_many_params() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_error_if_function_too_many_params";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    let line = interface.ereadln().unwrap();
    assert!(line.contains("Macro function 'error-if' expects 2 arguments, 3 given"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_lineno_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_lineno_function";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[ ] AN_INT: 64", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_macro_filename_function() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_filename_function";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert!(line.ends_with("tests/integration/menu/Kconfig_filename_function\""));
    assert!(line.starts_with("[ ] A_STRING: \""));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_unload() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[ ] BOOL: n", line);
    interface.writeln("unload").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_bool() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    eprintln!("{dotconfig}");
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update BOOL").unwrap();
    let _ = interface.readln().unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] BOOL: y", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_on_exit() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    eprintln!("{dotconfig}");
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update BOOL").unwrap();
    let _ = interface.readln().unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] BOOL: y", line);
    interface.writeln("exit").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("Do you want to save the current configuration?", line);
    let _ = interface.readln().unwrap();
    interface.writeln("yes").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
    assert!(PathBuf::from(dotconfig).is_file());
}

#[test]
fn test_load_and_do_not_save_on_exit() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    eprintln!("{dotconfig}");
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update BOOL").unwrap();
    let _ = interface.readln().unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] BOOL: y", line);
    interface.writeln("exit").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("Do you want to save the current configuration?", line);
    let _ = interface.readln().unwrap();
    interface.writeln("no").unwrap();
    assert_exits_timely(repl_thread);
    assert!(!PathBuf::from(dotconfig).is_file());
}

#[test]
fn test_load_and_cancel_save_on_exit() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    eprintln!("{dotconfig}");
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update BOOL").unwrap();
    let _ = interface.readln().unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] BOOL: y", line);
    interface.writeln("exit").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("Do you want to save the current configuration?", line);
    let _ = interface.readln().unwrap();
    interface.writeln("cancel").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert!(!PathBuf::from(dotconfig).is_file());
    assert_eq!("[*] BOOL: y", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_tristate() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_tristate";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update TRISTATE").unwrap();
    let switch = interface.readln().unwrap();
    eprintln!("{switch}");
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] TRISTATE: m", line);
    interface.writeln("update TRISTATE").unwrap();
    let switch = interface.readln().unwrap();
    eprintln!("{switch}");
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] TRISTATE: y", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_hex() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_hex";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update HEX").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    interface.writeln("0xff").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] HEX: 0xff", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_int() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_int";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update INT").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    interface.writeln("100").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] INT: 100", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_string() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_string";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update STRING").unwrap();
    loop {
        let line = interface.readln().unwrap();
        eprintln!("{line}");
        if line == "⮚ Please enter new value, end the sequence with two empty newlines:" {
            break;
        }
    }
    interface.writeln("Bar").unwrap();
    interface.writeln("").unwrap();
    interface.writeln("").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] STRING: \"Bar\"", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_save_string_multi_line() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_string";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update STRING").unwrap();
    loop {
        let line = interface.readln().unwrap();
        eprintln!("{line}");
        if line == "⮚ Please enter new value, end the sequence with two empty newlines:" {
            break;
        }
    }
    interface.writeln("Foo").unwrap();
    interface.writeln("Bar").unwrap();
    interface.writeln("").unwrap();
    interface.writeln("").unwrap();
    let line = interface.readln().unwrap();
    eprintln!("{line}");
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[*] STRING: \"Foo\\nBar\"", line);
    interface.writeln("save").unwrap();
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_load_and_reload_with_noprompt() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_tristate";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("update TRISTATE").unwrap();
    let switch = interface.readln().unwrap();
    eprintln!("{switch}");
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load --noprompt").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[ ] BOOL: n", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_navigate_up_down() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_submenu";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("Sub menu -->", line);
    interface.writeln("chmenu Sub menu").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("[ ] BOOL: n", line);
    interface.writeln("chmenu ..").unwrap();
    interface.writeln("list").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("Sub menu -->", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_info() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_bool";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    interface.writeln("info BOOL").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("Sets a boolean value", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_end_of_text_input() {
    let (interface, repl_thread) = setup_test();
    interface.writeln("config-set end-of-text-input").unwrap();
    interface.writeln("EOL").unwrap();
    interface.writeln("config-get end-of-text-input").unwrap();
    let line = interface.readln().unwrap();
    assert_eq!("EOL", line);
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_kconfig_does_not_exist() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_doesnotexist";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    assert!(interface
        .ereadln()
        .unwrap()
        .starts_with("Could not load tests/integration/menu/Kconfig_doesnotexist"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_kconfig_ast_error() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_ast_error";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    assert!(interface
        .ereadln()
        .unwrap()
        .starts_with("Keyword not expected"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

#[test]
fn test_kconfig_source_error() {
    let (interface, repl_thread) = setup_test();
    let kconfig = "tests/integration/menu/Kconfig_source_error";
    let dotconfig = generate_dotconfig();
    interface.writeln("config-set input").unwrap();
    interface.writeln(kconfig).unwrap();
    interface.writeln("config-set output").unwrap();
    interface.writeln(&dotconfig).unwrap();
    interface.writeln("load").unwrap();
    assert!(interface
        .ereadln()
        .unwrap()
        .starts_with("Erroneous element:"));
    interface.writeln("exit").unwrap();
    assert_exits_timely(repl_thread);
}

fn setup_test() -> (repl::TestInterface, thread::JoinHandle<()>) {
    let (terminal, interface) = test_repl();
    let thread = thread::spawn(move || {
        repl::run(&repl::NoPrompt {}, &terminal).unwrap();
    });
    (interface, thread)
}

fn assert_exits_timely(thread: thread::JoinHandle<()>) {
    let start_time = SystemTime::now();
    let max_time = start_time + Duration::new(5, 0);

    let finished_timely = loop {
        match thread.is_finished() {
            true => break true,
            false => {
                let now = SystemTime::now();
                if max_time < now {
                    break false;
                } else {
                    thread::sleep(Duration::new(0, 50));
                }
            }
        }
    };
    assert!(finished_timely);
}

fn generate_dotconfig() -> String {
    let time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis();

    let thread_id = thread::current().id();

    let filename = format!(".config-{time}-{:?}", thread_id);

    let mut dotconfig_file = env::temp_dir();
    dotconfig_file.push(filename);

    if dotconfig_file.exists() {
        // In the case a new test occurs within the same recorded "time" within
        // the same thread, yes it happens...
        generate_dotconfig()
    } else {
        let dotconfig = dotconfig_file.to_str().unwrap().to_string();
        eprintln!("Using .config alternative @ {}", &dotconfig);
        dotconfig
    }
}
