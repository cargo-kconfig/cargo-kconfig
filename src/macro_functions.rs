/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

use std::{collections::VecDeque, process::Command};

use kconfig_parser::{
    escape_string,
    lex::{structs::Lexicon, LexerBase},
    MacroLexer,
};
use kconfig_represent::Variant;

use crate::util::canonical_to_relative;

/// This trait must be implemented by any user of the macro functions below
pub trait FunctionConsoleOutput {
    /// Writes the given text to the output
    fn writeln(&self, s: &str);
    /// Writes the given text to the error output
    fn ewriteln(&self, s: &str);
}

pub fn load_function_macros<T, U>(lexer: &mut MacroLexer<T, U>)
where
    T: LexerBase,
    U: FunctionConsoleOutput + Clone,
{
    lexer.add_fn("shell", shell_function);
    lexer.add_fn("info", info_function);
    lexer.add_fn("warning-if", warning_if_function);
    lexer.add_fn("error-if", error_if_function);
    lexer.add_fn("lineno", lineno_function);
    lexer.add_fn("filename", filename_function);
}

fn shell_function<U>(
    args: Vec<String>,
    _filename: &str,
    _line: usize,
    _column: usize,
    _console_output: &U,
) -> Result<Vec<Lexicon>, String>
where
    U: FunctionConsoleOutput,
{
    if args.len() == 1 {
        let mut params: VecDeque<String> =
            args[0].split_whitespace().map(|s| s.to_string()).collect();

        if let Some(cmd) = params.pop_front() {
            match Command::new(cmd).args(params).output() {
                Ok(result) => match String::from_utf8(result.clone().stdout) {
                    Ok(stdout_result) => {
                        let result = stdout_result
                            .lines()
                            .map(|s| s.trim().to_string())
                            .collect::<Vec<String>>()
                            .join(" ");
                        Ok(vec![Lexicon::String(result)])
                    }
                    Err(e) => Err(format!(
                        "Macro function 'shell' failed to interpret output: {e}"
                    )),
                },
                Err(e) => Err(format!(
                    "Macro function 'shell' failed to execute command: {e}"
                )),
            }
        } else {
            Err(format!(
                "Macro function 'shell' parameter should contain at least 1 token, {} given",
                params.len()
            ))
        }
    } else {
        Err(format!(
            "Macro function 'shell' expects 1 argument, {} given",
            args.len()
        ))
    }
}

fn info_function<U>(
    args: Vec<String>,
    _filename: &str,
    _line: usize,
    _column: usize,
    console_output: &U,
) -> Result<Vec<Lexicon>, String>
where
    U: FunctionConsoleOutput,
{
    if args.len() == 1 {
        let s = format!("{}", args[0].trim());
        console_output.writeln(&s);
        Ok(vec![])
    } else {
        let err_s = format!(
            "Macro function 'info' expects 1 argument, {} given",
            args.len()
        );
        Err(err_s)
    }
}

fn warning_if_function<U>(
    args: Vec<String>,
    filename: &str,
    line: usize,
    _column: usize,
    console_output: &U,
) -> Result<Vec<Lexicon>, String>
where
    U: FunctionConsoleOutput,
{
    let filename = canonical_to_relative(filename);
    if args.len() == 2 {
        if Variant::parse(&args[0]) == Variant::from(true) {
            let s = format!("{filename}:{line} {}", args[1].trim());
            console_output.ewriteln(&s);
        }
        Ok(vec![])
    } else {
        Err(format!(
            "Macro function 'warning-if' expects 2 arguments, {} given",
            args.len()
        ))
    }
}

fn error_if_function<U>(
    args: Vec<String>,
    filename: &str,
    line: usize,
    _column: usize,
    _console_output: &U,
) -> Result<Vec<Lexicon>, String>
where
    U: FunctionConsoleOutput,
{
    let filename = canonical_to_relative(filename);
    if args.len() == 2 {
        if Variant::parse(&args[0]) == Variant::from(true) {
            let s = format!("{filename}:{line} {}", args[1].trim());
            Err(s)
        } else {
            Ok(vec![])
        }
    } else {
        Err(format!(
            "Macro function 'error-if' expects 2 arguments, {} given",
            args.len()
        ))
    }
}

fn lineno_function<U>(
    args: Vec<String>,
    _filename: &str,
    line: usize,
    _column: usize,
    _console_output: &U,
) -> Result<Vec<Lexicon>, String>
where
    U: FunctionConsoleOutput,
{
    if args.len() == 0 {
        Ok(vec![Lexicon::Identifier(line.to_string())])
    } else {
        let err_s = format!(
            "Macro function 'lineno' expects 0 arguments, {} given",
            args.len()
        );
        Err(err_s)
    }
}

fn filename_function<U>(
    args: Vec<String>,
    filename: &str,
    _line: usize,
    _column: usize,
    _console_output: &U,
) -> Result<Vec<Lexicon>, String>
where
    U: FunctionConsoleOutput,
{
    if args.len() == 0 {
        let escaped = escape_string(filename);
        Ok(vec![Lexicon::String(escaped)])
    } else {
        let err_s = format!(
            "Macro function 'filename' expects 0 arguments, {} given",
            args.len()
        );
        Err(err_s)
    }
}

#[cfg(test)]
mod test {
    use std::{cell::RefCell, rc::Rc};

    use kconfig_parser::lex::structs::Lexicon;

    use crate::macro_functions::{error_if_function, filename_function, lineno_function};

    use super::{warning_if_function, FunctionConsoleOutput};

    struct TestOutput {
        lines: Rc<RefCell<Vec<String>>>,
    }

    #[test]
    fn test_test_output() {
        let console_output = TestOutput::new();
        console_output.writeln("Foo");
        assert_eq!(1, console_output.len());
        assert_eq!("Foo", console_output.line(0));

        let console_output = TestOutput::new();
        console_output.ewriteln("Foo");
        assert_eq!(1, console_output.len());
        assert_eq!("Foo", console_output.line(0));
    }

    #[test]
    fn test_warning_if_function_no_warning() {
        let console_output = TestOutput::new();
        let result = warning_if_function(
            vec!["n".to_string(), "foo".to_string()],
            "foo",
            1,
            1,
            &console_output,
        );

        assert_eq!(0, result.unwrap().len());
        assert_eq!(0, console_output.len());
    }

    #[test]
    fn test_warning_if_function() {
        let console_output = TestOutput::new();
        let result = warning_if_function(
            vec!["y".to_string(), "foo".to_string()],
            "foo",
            1,
            1,
            &console_output,
        );

        assert_eq!(0, result.unwrap().len());
        assert_eq!(1, console_output.len());
    }

    #[test]
    fn test_error_if_function_no_error() {
        let console_output = TestOutput::new();
        let result = error_if_function(
            vec!["n".to_string(), "foo".to_string()],
            "foo",
            1,
            1,
            &console_output,
        );

        assert_eq!(0, result.unwrap().len());
        assert_eq!(0, console_output.len());
    }

    #[test]
    fn test_error_if_function() {
        let console_output = TestOutput::new();
        let result = error_if_function(
            vec!["y".to_string(), "foo".to_string()],
            "foo",
            1,
            1,
            &console_output,
        );

        result.unwrap_err();
        assert_eq!(0, console_output.len());
    }

    #[test]
    fn test_lineno_function_no_error() {
        let console_output = TestOutput::new();
        let result = lineno_function(vec![], "foo", 1, 1, &console_output);
        let v = result.unwrap();
        assert_eq!(1, v.len());
        assert_eq!(Lexicon::Identifier("1".to_owned()), v[0]);
        assert_eq!(0, console_output.len());
    }

    #[test]
    fn test_lineno_function_too_many_arguments() {
        let console_output = TestOutput::new();
        let result = lineno_function(vec!["foo".to_owned()], "foo", 1, 1, &console_output);
        result.unwrap_err();
        assert_eq!(0, console_output.len());
    }

    #[test]
    fn test_filename_function_no_error() {
        let console_output = TestOutput::new();
        let result = filename_function(vec![], "foo", 1, 1, &console_output);
        let v = result.unwrap();
        assert_eq!(1, v.len());
        assert_eq!(Lexicon::String("\"foo\"".to_owned()), v[0]);
        assert_eq!(0, console_output.len());
    }

    #[test]
    fn test_filename_function_too_many_arguments() {
        let console_output = TestOutput::new();
        let result = filename_function(vec!["foo".to_owned()], "foo", 1, 1, &console_output);
        result.unwrap_err();
        assert_eq!(0, console_output.len());
    }

    #[cfg(target_os = "linux")]
    #[test]
    fn test_shell_function() {
        use crate::macro_functions::shell_function;

        let console_output = TestOutput::new();
        let result = shell_function(vec!["sh -c pwd".to_string()], "foo", 1, 1, &console_output);
        let shell_result = result.unwrap();
        assert_eq!(1, shell_result.len());
        let output = shell_result[0].to_string();
        let cwd = std::env::current_dir().unwrap();
        let cwd = cwd.to_str().unwrap();
        assert_eq!(cwd, output);
    }

    #[test]
    fn test_shell_function_invalid_command() {
        use crate::macro_functions::shell_function;

        let console_output = TestOutput::new();
        let result = shell_function(
            vec!["invalid-command-which-should-not-exist".to_string()],
            "foo",
            1,
            1,
            &console_output,
        );
        let result_err = result.unwrap_err();
        assert!(result_err.contains("Macro function 'shell' failed to execute command"));
    }

    #[test]
    fn test_shell_function_no_command() {
        use crate::macro_functions::shell_function;

        let console_output = TestOutput::new();
        let result = shell_function(vec!["".to_string()], "foo", 1, 1, &console_output);
        let result_err = result.unwrap_err();
        assert!(result_err
            .contains("Macro function 'shell' parameter should contain at least 1 token, 0 given"));
    }

    #[test]
    fn test_shell_function_not_enough_arguments() {
        use crate::macro_functions::shell_function;

        let console_output = TestOutput::new();
        let result = shell_function(vec![], "foo", 1, 1, &console_output);
        let result_err = result.unwrap_err();
        assert!(result_err.contains("Macro function 'shell' expects 1 argument, 0 given"));
    }

    #[test]
    fn test_shell_function_too_many_arguments() {
        use crate::macro_functions::shell_function;

        let console_output = TestOutput::new();
        let result = shell_function(
            vec!["foo".to_owned(), "bar".to_owned()],
            "foo",
            1,
            1,
            &console_output,
        );
        let result_err = result.unwrap_err();
        assert!(result_err.contains("Macro function 'shell' expects 1 argument, 2 given"));
    }

    impl TestOutput {
        fn new() -> Self {
            Self {
                lines: Rc::new(RefCell::new(Vec::new())),
            }
        }

        fn len(&self) -> usize {
            self.lines.borrow().len()
        }

        fn line(self, i: usize) -> String {
            self.lines.borrow().get(i).cloned().unwrap_or_default()
        }
    }

    impl FunctionConsoleOutput for TestOutput {
        fn writeln(&self, s: &str) {
            self.lines.borrow_mut().push(s.to_string())
        }

        fn ewriteln(&self, s: &str) {
            self.lines.borrow_mut().push(s.to_string())
        }
    }
}
