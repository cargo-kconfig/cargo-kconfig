/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use proc_macro::{Delimiter, Group, Ident, Literal, Span, TokenStream, TokenTree};

use super::{
    eval::Eval,
    util::{must_be_assign, must_be_comma, next_token},
};

pub(crate) fn run(value_mode: ValueMode, input: TokenStream) -> TokenStream {
    let mut iter = input.into_iter();

    let mut v = Vec::<TokenTree>::new();
    let mut tt = next_token(&mut iter);

    let tag_name = match tt {
        proc_macro::TokenTree::Ident(i) => {
            if i.to_string() == "tag" {
                tt = next_token(&mut iter);

                must_be_assign(&tt);

                tt = next_token(&mut iter);

                let tag_name = match tt {
                    TokenTree::Ident(i) => i.to_string(),
                    _ => panic!("Expected identifier with tag name"),
                };

                tt = next_token(&mut iter);
                must_be_comma(&tt);

                tag_name
            } else {
                let backup = Ident::new(&i.to_string(), i.span());
                v.push(TokenTree::Ident(backup));
                "default".to_owned()
            }
        }
        _ => {
            v.push(tt);
            "default".to_owned()
        }
    };

    loop {
        match iter.next() {
            Some(tt) => v.push(tt),
            None => break,
        }
    }

    let group = Group::new(Delimiter::Parenthesis, v.into_iter().collect());
    let eval = Eval::new_tagged(&tag_name, &group).eval();
    let value = eval.as_value();

    let tt = match value_mode {
        ValueMode::Int => TokenTree::Literal(Literal::i64_suffixed(value.into())),
        ValueMode::String => TokenTree::Literal(Literal::string(&value.to_string())),
        ValueMode::Bool => {
            let bool_value = if value.into() { "true" } else { "false" };
            TokenTree::Ident(Ident::new(bool_value, Span::call_site()))
        }
    };
    vec![tt].into_iter().collect()
}

pub(crate) enum ValueMode {
    Int,
    String,
    Bool,
}
