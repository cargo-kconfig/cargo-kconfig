/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use proc_macro::{token_stream::IntoIter, TokenTree};

pub(crate) fn next_token(iter: &mut IntoIter) -> TokenTree {
    match iter.next() {
        Some(tt) => tt,
        None => panic!("End of input not expected"),
    }
}

pub(crate) fn is_comma(tt: &TokenTree) -> bool {
    match tt {
        TokenTree::Punct(p) => {
            if p.as_char() == ',' {
                true
            } else {
                false
            }
        }
        _ => false,
    }
}

pub(crate) fn must_be_assign(tt: &TokenTree) {
    match tt {
        TokenTree::Punct(p) => {
            if p.as_char() == '=' {
            } else {
                panic!("Expected '='")
            }
        }
        _ => panic!("Expected '='"),
    }
}

pub(crate) fn must_be_comma(tt: &TokenTree) {
    match tt {
        TokenTree::Punct(p) => {
            if p.as_char() == ',' {
            } else {
                panic!("Expected ','")
            }
        }
        _ => panic!("Expected ','"),
    }
}

/// Parses a string value and removes escapes
pub(crate) fn parse_string(value: &str) -> String {
    let mut chars = value.chars();
    // Trim the left and right double quotes
    if value.len() < 2 || chars.next() != Some('\"') || chars.last() != Some('\"') {
        panic!(
            "Could not find starting or ending double quote in: {}",
            value
        );
    }
    let basic = value[1..value.len() - 1].chars();

    // Build the string into result
    let mut result: String = "".to_string();

    // If the state is Normal, then it is a normal character, which is appended
    // into result, if the state is Escape, then an escape character (\) has been
    // found prior to the current character, and the character needs unescaping.
    enum State {
        Normal,
        Escaped,
    }
    let mut state = State::Normal;
    for c in basic {
        match state {
            State::Normal => match c {
                '\\' => state = State::Escaped,
                _ => result += &format!("{}", c),
            },
            State::Escaped => {
                match c {
                    '"' => result += "\"",
                    'n' => result += "\n",
                    'r' => result += "\r",
                    't' => result += "\t",
                    '\\' => result += "\\",
                    _ => panic!("Expected escape symbol, found: {}", c),
                };
                state = State::Normal;
            }
        }
    }
    // If the state remained into escaped, it means that the last character has not
    // been properly closed.
    if let State::Escaped = state {
        panic!("Expected input to be closed, found escape token: \\");
    }
    result
}
