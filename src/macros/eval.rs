/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use kconfig_represent::Variant;
use std::{fmt::Display, iter::Peekable, rc::Rc};

use kconfig_represent::ConfigRegistry;
use proc_macro::{token_stream::IntoIter, Delimiter, Group, Spacing, TokenTree};

use super::{comp::Comparison, tag_registry::TagRegistry, util::parse_string};

#[derive(Clone)]
pub(crate) struct Eval {
    config_registry: Rc<ConfigRegistry>,
    group: Group,
}

impl Eval {
    pub(crate) fn new_tagged(tag_name: &str, group: &Group) -> Self {
        let config_registry = TagRegistry::get_config_registry(tag_name);
        Self::new(&config_registry, group)
    }

    pub(crate) fn new(config_registry: &Rc<ConfigRegistry>, group: &Group) -> Self {
        Self {
            config_registry: config_registry.clone(),
            group: group.clone(),
        }
    }

    pub(crate) fn eval(&self) -> ValueRef {
        match self.group.delimiter() {
            Delimiter::Parenthesis => {
                let tokens = self.group.stream();
                let unordered = self.evaluate_tokens(&mut tokens.into_iter().peekable());
                unordered.eval()
            }
            _ => panic!("Expected (, found {}", self.group.to_string()),
        }
    }

    fn evaluate_tokens(&self, iter: &mut Peekable<IntoIter>) -> Unordered {
        let lhs = match iter.next() {
            Some(tt) => match tt {
                TokenTree::Group(_) => self.eval(),
                TokenTree::Ident(i) => {
                    let s = i.to_string();
                    if s == "true" {
                        ValueRef::Boolean(true)
                    } else if s == "false" {
                        ValueRef::Boolean(false)
                    } else {
                        ValueRef::Config((s, self.config_registry.clone()))
                    }
                }
                TokenTree::Punct(p) => {
                    panic!(
                        "Expected group, identifier or literal, not: {}",
                        p.to_string()
                    )
                }
                TokenTree::Literal(l) => {
                    let content = l.to_string();
                    if content.starts_with('"') {
                        ValueRef::String(parse_string(&content))
                    } else {
                        match content.parse::<i64>() {
                            Ok(value) => ValueRef::Int(value),
                            _ => panic!("Literal value {} could not be parsed", content),
                        }
                    }
                }
            },
            None => panic!("End of input not expected"),
        };

        let comp = match iter.next() {
            Some(tt) => match tt {
                TokenTree::Punct(p) => match p.spacing() {
                    Spacing::Alone => Comparison::single(&p),
                    Spacing::Joint => match iter.peek() {
                        Some(peeked) => match peeked {
                            TokenTree::Punct(p2) => Comparison::dual(&p, p2),
                            _ => Comparison::single(&p),
                        },
                        None => Comparison::single(&p),
                    },
                },
                _ => panic!("Expected ==, !=, <, <=, >=, >, &&, ||"),
            },
            None => Comparison::None,
        };

        match comp {
            Comparison::Lt | Comparison::Gt | Comparison::None => (),
            // Since values have been peeked only in the case of dual
            // operator, now actually advance the iterator.
            _ => {
                iter.next();
                ()
            }
        }

        match comp {
            Comparison::None => Unordered::ValueRef(lhs),
            _ => {
                let rhs = self.evaluate_tokens(iter);
                Unordered::Link((lhs, comp), Box::new(rhs))
            }
        }
    }
}

#[derive(Clone, Debug)]
pub(crate) enum Unordered {
    ValueRef(ValueRef),
    Link((ValueRef, Comparison), Box<Unordered>),
}

impl Unordered {
    pub(crate) fn eval(&self) -> ValueRef {
        self.sort().eval()
    }

    pub(crate) fn sort(&self) -> Ordered {
        // algorithm:
        // 1) If the current value is a value reference,
        //    then no further ordering needs to take place, and
        //    the value reference will be directly copied
        // 2) If the current value is a link then:
        //    a) If the next unordered cons also contains
        //       a link, then if this link is lower then
        //       the next link, the next link is evaluated
        //       as a single group, and the current link is
        //       then reattached.
        //    b) otherwise, the current link is propagated as
        //       correctly ordered

        match self {
            Self::ValueRef(value_ref) => Ordered::ValueRef(value_ref.clone()),
            Self::Link((lhs, comp), boxed_rhs) => {
                let rhs = *boxed_rhs.clone();
                match &rhs {
                    Self::ValueRef(value_ref) =>
                    // It doesn't matter, this is the final stop
                    {
                        Ordered::Link(
                            Box::new(Ordered::ValueRef(lhs.clone())),
                            comp.clone(),
                            Box::new(Ordered::ValueRef(value_ref.clone())),
                        )
                    }
                    Self::Link((cdr_lhs, cdr_comp), cdr_rhs) => {
                        let is_higher = comp.higher(&cdr_comp);
                        match is_higher {
                            false => {
                                // Ordered LHS => LHS
                                // Ordered Comp => comp
                                // Ordered RHS => rhs
                                let lhs = Box::new(Ordered::ValueRef(lhs.clone()));
                                let rhs = Box::new(rhs.sort());
                                Ordered::Link(lhs, comp.clone(), rhs)
                            }
                            true => {
                                // Ordered LHS => lhs, comp, cdr_lhs
                                // Ordered Comp => cdr_comp
                                // Ordered RHS => cdr_rhs
                                let lhs = Box::new(Ordered::Link(
                                    Box::new(Ordered::ValueRef(lhs.clone())),
                                    comp.clone(),
                                    Box::new(Ordered::ValueRef(cdr_lhs.clone())),
                                ));
                                let rhs = Box::new(cdr_rhs.sort());
                                Ordered::Link(lhs, cdr_comp.clone(), rhs)
                            }
                        }
                    }
                }
            }
        }
    }
}

#[derive(Clone)]
pub(crate) enum Ordered {
    ValueRef(ValueRef),
    Link(Box<Ordered>, Comparison, Box<Ordered>),
}

impl Ordered {
    pub(crate) fn eval(&self) -> ValueRef {
        match self {
            Self::ValueRef(value_ref) => value_ref.clone(),
            Self::Link(boxed_lhs, comp, boxed_rhs) => {
                comp.compare(boxed_lhs.eval(), boxed_rhs.eval())
            }
        }
    }
}

impl Display for Ordered {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ValueRef(value_ref) => write!(f, "{value_ref}"),
            Self::Link(lhs, comparison, rhs) => write!(f, "({lhs} {comparison} {rhs})"),
        }
    }
}

#[derive(Clone, Debug)]
pub(crate) enum ValueRef {
    // Extracted from an identifier (unless exactly "true" or "false")
    Config((String, Rc<ConfigRegistry>)),

    // Extracted from an identifier
    Boolean(bool),

    // Extracted from a literal either a string or integer value
    String(String),
    Int(i64),
}

impl Display for ValueRef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Config((symbol, _)) => write!(f, "{symbol}"),
            Self::Boolean(b) => write!(f, "{b}"),
            Self::String(s) => write!(f, "{s}"),
            Self::Int(i) => write!(f, "{i}"),
        }
    }
}

impl ValueRef {
    pub(crate) fn as_value(&self) -> Variant {
        match self {
            ValueRef::Config((symbol, config_registry)) => {
                if let Some(itemref) = config_registry.find_itemref(symbol) {
                    if let Some(value) = config_registry.current_value(&itemref) {
                        value
                    } else {
                        panic!("No value assigned to symbol '{symbol}'");
                    }
                } else {
                    panic!("No value assigned to symbol '{symbol}'");
                }
            }
            ValueRef::Boolean(b) => Variant::from(b.clone()),
            ValueRef::String(s) => Variant::from(s.clone()),
            ValueRef::Int(i) => Variant::from(i.clone()),
        }
    }
}
