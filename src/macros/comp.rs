/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use std::fmt::Display;

use kconfig_represent::Variant;
use proc_macro::Punct;

use super::eval::ValueRef;

#[derive(Clone, Copy, Debug)]
pub(crate) enum Comparison {
    Lt,
    Gt,
    Lte,
    Gte,
    Eq,
    Ne,
    And,
    Or,
    None,
}

impl Comparison {
    pub(crate) fn compare(&self, lhs: ValueRef, rhs: ValueRef) -> ValueRef {
        let lhs = lhs.as_value();
        let rhs = rhs.as_value();

        ValueRef::Boolean(match self {
            // Note that comparing a < b should be the same as comparing b > a,
            // so in stead of creating a specific function for "<" comparison,
            // reuse the existing ">" function.
            Self::Lt => lhs < rhs,
            Self::Gt => lhs > rhs,
            Self::Lte => lhs <= rhs,
            Self::Gte => lhs >= rhs,
            Self::Eq => lhs == rhs,
            Self::Ne => lhs != rhs,
            Self::And => Self::and(lhs, rhs),
            Self::Or => Self::or(lhs, rhs),
            Self::None => false,
        })
    }

    fn and(lhs: Variant, rhs: Variant) -> bool {
        lhs.to_bool() && rhs.to_bool()
    }

    fn or(lhs: Variant, rhs: Variant) -> bool {
        lhs.to_bool() || rhs.to_bool()
    }

    pub(crate) fn single(p: &Punct) -> Self {
        match p.as_char() {
            '<' => Self::Lt,
            '>' => Self::Gt,
            _ => panic!("Operator {} invalid", p.as_char()),
        }
    }

    pub(crate) fn dual(p1: &Punct, p2: &Punct) -> Self {
        match (p1.as_char(), p2.as_char()) {
            ('<', '=') => Self::Lte,
            ('>', '=') => Self::Gte,
            ('=', '=') => Self::Eq,
            ('!', '=') => Self::Ne,
            ('&', '&') => Self::And,
            ('|', '|') => Self::Or,
            _ => panic!("Operator {}{} invalid", p1.as_char(), p2.as_char()),
        }
    }

    pub(crate) fn higher(&self, other: &Self) -> bool {
        self.level() > other.level()
    }

    fn level(&self) -> i8 {
        match self {
            Self::Lt | Self::Gt | Self::Lte | Self::Gte | Self::Eq | Self::Ne => 3,
            Self::And => 2,
            Self::Or => 1,
            _ => 0,
        }
    }
}

impl Display for Comparison {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Comparison::Lt => write!(f, "<"),
            Comparison::Gt => write!(f, ">"),
            Comparison::Lte => write!(f, "<="),
            Comparison::Gte => write!(f, ">="),
            Comparison::Eq => write!(f, "=="),
            Comparison::Ne => write!(f, "!="),
            Comparison::And => write!(f, "&&"),
            Comparison::Or => write!(f, "||"),
            Comparison::None => write!(f, "!!!"),
        }
    }
}
