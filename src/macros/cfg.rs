/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use proc_macro::{Delimiter, Group, Ident, TokenStream, TokenTree};

use super::{
    eval::Eval,
    util::{is_comma, must_be_assign, must_be_comma, next_token},
};

/// There are four ways the run can be invoked:
///
/// <config-expression>, <rust-expression>
/// <config-expression>, <success-expression>, <failure-expression>
/// tag = <name>, <config-expression>, <rust-expression>
/// tag = <name>, <config-expression>, <success-expression>, <failure-expression>
pub(crate) fn run(input: TokenStream) -> TokenStream {
    let mut iter = input.into_iter();
    let tt = next_token(&mut iter);

    let eval_success = eval(tt, &mut iter);

    let mut result_ok = Vec::<TokenTree>::new();
    let mut result_nok = Vec::<TokenTree>::new();

    let mut first_arm = true;

    loop {
        let next_token = iter.next();
        match next_token {
            Some(tt) => match tt {
                TokenTree::Punct(punct) => match punct.as_char() {
                    ',' => match first_arm {
                        true => first_arm = false,
                        false => panic!("Tertiary evaluation arm not supported"),
                    },
                    _ => match first_arm {
                        true => result_ok.push(TokenTree::Punct(punct)),
                        false => result_nok.push(TokenTree::Punct(punct)),
                    },
                },
                _ => match first_arm {
                    true => result_ok.push(tt),
                    false => result_nok.push(tt),
                },
            },
            None => break,
        }
    }

    match eval_success {
        true => result_ok.into_iter().collect(),
        false => result_nok.into_iter().collect(),
    }
}

fn eval(mut tt: TokenTree, iter: &mut proc_macro::token_stream::IntoIter) -> bool {
    match tt {
        // if the tokentree starts with a group, and state
        // equals start, then this is a tag-less invocation
        TokenTree::Group(g) => {
            let eval = Eval::new_tagged("default", &g);
            let result = eval.eval().as_value().to_bool();
            tt = next_token(iter);
            must_be_comma(&tt);
            if !result {
                return false;
            }
        }

        // If the tokentree starts with an identifier, then this
        // can be a tagged expression, only if the identifier
        // equals "tag"
        TokenTree::Ident(i) => {
            let mut v = Vec::<TokenTree>::new();

            let tag_name = if i.to_string() == "tag" {
                tt = next_token(iter);

                must_be_assign(&tt);

                tt = next_token(iter);

                let tag_name = match tt {
                    TokenTree::Ident(i) => i.to_string(),
                    _ => panic!("Expected identifier with tag name"),
                };

                tt = next_token(iter);
                must_be_comma(&tt);

                tag_name
            } else {
                let backup = Ident::new(&i.to_string(), i.span());
                v.push(TokenTree::Ident(backup));
                "default".to_owned()
            };

            loop {
                tt = next_token(iter);
                if is_comma(&tt) {
                    break;
                }
                v.push(tt);
            }

            let mut s = TokenStream::new();
            s.extend(v.into_iter());

            let group = Group::new(Delimiter::Parenthesis, s);
            let eval = Eval::new_tagged(&tag_name, &group);
            if !eval.eval().as_value().to_bool() {
                return false;
            }
        }

        TokenTree::Punct(_) | TokenTree::Literal(_) => {
            if is_comma(&tt) {
                panic!("Expected tag assignment or expression, not comma")
            }
            let mut v = Vec::<TokenTree>::new();
            v.push(tt);
            loop {
                tt = next_token(iter);
                if is_comma(&tt) {
                    break;
                }
                v.push(tt);
            }

            let mut s = TokenStream::new();
            s.extend(v.into_iter());

            let group = Group::new(Delimiter::Parenthesis, s);
            let eval = Eval::new_tagged("default", &group);
            if !eval.eval().as_value().to_bool() {
                return false;
            }
        }
    }
    true
}
