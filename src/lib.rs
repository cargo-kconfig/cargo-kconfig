/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

use macros::value::ValueMode;
use proc_macro::TokenStream;
mod macro_functions;
mod macros;
mod util;

/// This macro loads a configuration. By default, it loads the configuration
/// from current working directory, and tags it as default configuration.
/// However, this can be manipulated in mulitple ways:
///
/// * If the argument `tag=<name>` is supplied, the loaded configuration
///   is tagged using the supplied name.
///
/// * If the argument `kconfig_path="<path>"` is supplied, instead of
///   attempting to load the Kconfig file from the current working directory,
///   it is loaded from the given directory.
///
/// * If the argument `kconfig_file="<file>"` is supplied, instead of
///   attempting to load the Kconfig file, an alternate file name is used.
///
/// * If the argument `dotconfig_path="<path>"` is supplied, instead of
///   attempting to load the .config file from the current working directory,
///   it is loaded from the given directory.
///
/// * If the argument `dotconfig_file="<file>"` is supplied, instead of
///   attempting to load the .config gile, an alternate file name is used.
///
/// # Example
///
/// The following construct will load a configuration with the Kconfig
/// file located in the subdirectory _foo_, and the .config filename
/// is substituted with _.bar_.
///
///     load_cfg!(tag=foo, kconfig_path="./foo", dotconfig_file=".bar");
#[proc_macro]
pub fn load_kcfg(input: TokenStream) -> TokenStream {
    crate::macros::load::run(input)
}

/// This macro reports an existing configuration. By default, it reports
/// the configuration with the tag name "default". If this configuration
/// is not available
#[proc_macro]
pub fn kcfg_report(input: TokenStream) -> TokenStream {
    crate::macros::report::run(input)
}

/// Given a configuration, renders or does not render the given statement
/// or expression.
///
/// * If the argument `tag=<name>` is the first argument set, prior to
///   any other argument, then instead of using the 'default' tag, the
///   given tag is used.
///
/// * The argument after the tag argument, separated by a comma (where
///   applicable), is the condition. This condition contains a DSL,
///   combining the names of the configuration, literals and identifiers
///   as operands. All identifiers, except "true" and "false" are
///   considered to be configuration items within the Kconfig and it's
///   associated .config file. Other literals (numbers and strings) as
///   well as true and false are considered as direct operands.
///   Comparison can be performed by utilizing the common operators such
///   as ==, !=, <, >, <= and >=, further && and || can be used as
///   operators. When using && and ||, the expressions before and after
///   are parsed as boolean. Further, there is no operator-precedence
///   defined.
///
/// * The argument after the condition should contain the desired
///   expression or statement to allow, if the condition is met.
///
/// * If another argument is given after the previous argument, it
///   should contain the expression or statement to allow, if the
///   condition is not met.
///
/// Example:
///
///     if kcfg!(FOO == "bar", true, false) {
///         ...
///     }
///
/// Another example:
///
///     kcfg!(FOO == "bar", println!("FOO equals 'bar'"););
#[proc_macro]
pub fn kcfg(input: TokenStream) -> TokenStream {
    crate::macros::cfg::run(input)
}

/// Given a configuration, returns the value of the expression as
/// boolean value.
///
/// * If the argument `tag=<name>` is the first argument set, prior to
///   any other argument, then instead of using the 'default' tag, the
///   given tag is used.
///
/// * The argument after the tag argument, separated by a comma (where
///   applicable), is the expression. It is evaluated the same as a
///   condition within the kcfg macro.
///
/// Example:
///
///     if kbool!(FOO) {
///         ....
///     }
#[proc_macro]
pub fn kbool(input: TokenStream) -> TokenStream {
    crate::macros::value::run(ValueMode::Bool, input)
}

/// Given a configuration, returns the value of the expression as
/// unsigned integer.
///
/// * If the argument `tag=<name>` is the first argument set, prior to
///   any other argument, then instead of using the 'default' tag, the
///   given tag is used.
///
/// * The argument after the tag argument, separated by a comma (where
///   applicable), is the expression. It is evaluated the same as a
///   condition within the kcfg macro.
///
/// Example:
///
///     if kint!(FOO) == 0 {
///         ....
///     }
#[proc_macro]
pub fn kint(input: TokenStream) -> TokenStream {
    crate::macros::value::run(ValueMode::Int, input)
}

/// Given a configuration, returns the value of the expression as
/// string.
///
/// * If the argument `tag=<name>` is the first argument set, prior to
///   any other argument, then instead of using the 'default' tag, the
///   given tag is used.
///
/// * The argument after the tag argument, separated by a comma (where
///   applicable), is the expression. It is evaluated the same as a
///   condition within the kcfg macro.
///
/// Example:
///
///     if kstr!(FOO) == "bar" {
///         ....
///     }
#[proc_macro]
pub fn kstr(input: TokenStream) -> TokenStream {
    crate::macros::value::run(ValueMode::String, input)
}
