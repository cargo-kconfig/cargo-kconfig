/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use crate::macro_functions::{load_function_macros, FunctionConsoleOutput};
use crate::util::determine_file_name;
use crate::util::lex_from_file;
use crate::util::load_dot_config_into_registry;
use kconfig_parser::MacroLexer;
use kconfig_parser::{escape_string, Ast};
use kconfig_represent::LoadError;
use kconfig_represent::MutationStart;
use kconfig_represent::Variant;
use kconfig_represent::{
    dependencies::{ItemDependency, ItemExpr},
    ConfigRegistry,
};
use kconfig_represent::{ItemReference, VariantDataType};
use std::cell::RefCell;
use std::fmt::Display;
use std::fs::File;
use std::path::PathBuf;
use std::rc::Rc;

/// A route is an outcome of a parsed input, and executes a certain action.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Routes {
    /// The config-get command retrieves the current config values
    /// for the working set of the Kconfig file and .config file amongst
    /// other settings. If no ConfigSetting has been specified, all settings
    /// will be displayed, otherwise only the given configuration setting
    /// will be displayed
    ConfigGet(Option<ConfigSetting>),

    /// The config-set command sets one of the current config values
    /// for the working set of the Kconfig file and .config file amongst
    /// other settings
    ConfigSet(ConfigSetting),

    /// The save command saves the the current configuration into the
    /// .config file
    Save,

    /// The load command loads the configuration from the Kconfig file and
    /// augments it with the already saved .config file if it exists. If
    /// the contained boolean value is true, then unsaved changes will be
    /// ignored, and no prompt will be shown.
    Load(bool),

    /// The unload command unloads the already loaded configuration, if any.
    /// If the contained boolean value is true, then unsaved changes will be
    /// ignored, and no prompt will be shown.
    Unload(bool),

    /// The list route will return a list of options belonging to the
    /// currently activated menu item.
    List,

    /// The change commands navigates up or down a menu
    ChMenu(Navigation),

    /// The Info Command shows the help text of a configuration item entry
    Info(String),

    /// The Update Command updates a configuration item's value with a new
    /// value. Depending on the kind of configuration item, the item can
    /// either be updated by shifting it's value, be updated by input
    /// through the terminal or by input through an editor
    Update(String),

    /// The Exit command exits the Read-Eval-Print-Loop. If the boolean
    /// is set to true, then exit the Read-Eval-Print-Loop without detection
    /// of changes to the loaded kconfig/.config files, and skip the
    /// save request
    Exit(bool),
}

/// Informs whether to navigate upwards to a previous menu item or menu
/// configuration item, or downwards. It is only possible to navigate down
/// towards a proper menu item or menu configuration item.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Navigation {
    /// Navigates upwards in the menu (configuration) items (if possible)
    Up,
    /// Navigates downwards in the menu (configuration) items, and contains
    /// the name of the item to navigate to.
    Down(String),
}

/// Selects the configuration setting to retrieve or alter
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ConfigSetting {
    /// Retrieves the input file name, typically the Kconfig file
    Input,
    /// Retrieves the output file name, typically the .config file
    Output,
    /// End of text input
    EndOfTextInput,
}

impl Display for ConfigSetting {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::Input => write!(f, "input"),
            Self::Output => write!(f, "output"),
            Self::EndOfTextInput => write!(f, "end-of-text-input"),
        }
    }
}

/// This structure represents the configuration set in the.cargo-kconfig
/// file, if available, otherwise represents the defaults.
pub struct Session {
    kconfig: PathBuf,
    dotconfig: PathBuf,
    end_of_text_input: String,
    changed: bool,
    activated_menus: Vec<ItemReference>,
    registry: Option<ConfigRegistry>,
}

impl Session {
    /// Returns the location of the Kconfig file, typically this would be
    /// the Kconfig file in the current working directory.
    pub fn kconfig(&self) -> PathBuf {
        self.kconfig.clone()
    }

    /// Returns the location of the .config file, typically this would be
    /// the .config file in the current working directory.
    pub fn dotconfig(&self) -> PathBuf {
        self.dotconfig.clone()
    }

    /// Returns the end of text input configuration. When empty, this indicates
    /// two empty new lines.
    pub fn end_of_text_input(&self) -> String {
        self.end_of_text_input.clone()
    }

    /// Returns the value of the given config configuration setting
    pub fn setting(&self, setting: &ConfigSetting) -> String {
        match setting {
            ConfigSetting::Input => self.kconfig().to_str().unwrap_or("").to_string(),
            ConfigSetting::Output => self.dotconfig().to_str().unwrap_or("").to_string(),
            ConfigSetting::EndOfTextInput => self.end_of_text_input().clone(),
        }
    }

    /// Sets the location of the Kconfig file, if it is to deviate from the
    /// default location in the current working directory.
    pub fn set_kconfig(&mut self, v: &PathBuf) {
        self.kconfig = v.clone()
    }

    /// Sets the location of the .config file, if it is to deviate from the
    /// default location in the current working directory.
    pub fn set_dotconfig(&mut self, v: &PathBuf) {
        self.dotconfig = v.clone()
    }

    /// Set the end of text input configuration, if none given, it implies
    /// two empty newlines.
    pub fn set_end_of_text_input(&mut self, s: &str) {
        self.end_of_text_input = s.to_string()
    }

    /// Sets the changed flag, which indicates that the current configuration
    /// has been changed after the last save of the configuration.
    pub fn flag_changed(&mut self) {
        self.changed = true;
    }

    /// Indicates whether the current configuration has been changed after
    /// the last save of the configuration.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Indicates whether the configuration registry has been loaded
    pub fn loaded(&self) -> bool {
        match self.registry {
            Some(_) => true,
            None => false,
        }
    }

    /// Unloads an already loaded configuration registry
    pub fn unload(&mut self) {
        if let Some(_) = self.registry {
            self.registry = None;
            self.changed = false;
        }
    }

    /// Loads the configuration into the registry. It optionally results
    /// into a load error, which indicates that certain configuration values
    /// found in the .config file, can not be resolved (anymore) into the
    /// configuration items found in the Kconfig file.
    ///
    /// If loading is not possible, a string will be returned containing
    /// the error description of the underlying error.
    pub fn load(&mut self) -> Result<SessionLoadInfo, String> {
        let mut load_error = None;
        let co = ConsoleOutput::new();
        if let Some(filename) = self.kconfig.to_str() {
            {
                if let Err(e) = File::open(determine_file_name(filename)) {
                    return Err(format!("Could not load {filename}: {e}"));
                }
            }

            let base_lexer = match lex_from_file(filename) {
                Ok(lexer) => lexer,
                Err(e) => {
                    return Err(format!("{e}"));
                }
            };

            let mut lexer = MacroLexer::new(base_lexer, &co);

            load_function_macros(&mut lexer);

            let ast = match Ast::parse(&mut lexer) {
                Ok(ast) => ast,
                Err(e) => {
                    return Err(format!("{e}"));
                }
            };

            let mut registry = match ConfigRegistry::new(&ast, &lexer.symbol_table()) {
                Ok(registry) => registry,
                Err(e) => {
                    return Err(format!("{e}"));
                }
            };
            if let Some(dotconfig_filename) = self.dotconfig.to_str() {
                load_error = load_dot_config_into_registry(dotconfig_filename, &mut registry);
            }
            self.registry = Some(registry);
            self.activated_menus = vec![];
        }

        Ok(SessionLoadInfo {
            load_transcript: co.transcript(),
            maybe_load_error: load_error,
        })
    }

    /// Saves the configuration registry to file
    pub fn save(&mut self) {
        if let Some(registry) = &mut self.registry {
            if let Some(filename) = self.dotconfig.to_str() {
                registry.write_dotconfig_file(filename).unwrap();
            }
            self.changed = false;
        }
    }

    /// Lists the subitems of the currently activated menu item or menu
    /// configurtion item and returns them as strings.
    pub fn list(&self) -> Result<Vec<PrintableListItem>, String> {
        if let Some(registry) = &self.registry {
            let mut result = Vec::new();
            for itemref in self.list_itemrefs() {
                if let Some(item) = registry.get(&itemref) {
                    let name = item.name().to_string();
                    let value = registry.current_value(&itemref);
                    let is_default = registry.current_value_is_default(&itemref);
                    let itemtype = match &itemref {
                        ItemReference::Config(_) => ListItemType::Config,
                        ItemReference::MenuConfig(_) => ListItemType::MenuConfig,
                        ItemReference::Menu(_) => ListItemType::Menu,
                    };
                    result.push(PrintableListItem {
                        name,
                        value,
                        is_default,
                        itemtype,
                    });
                }
            }
            Ok(result)
        } else {
            Err(format!("Currently no configuration loaded"))
        }
    }

    /// Retrieves the "help" text from the given info item
    pub fn info(&self, name: &str) -> Result<Box<dyn Iterator<Item = String>>, String> {
        if let Some(registry) = &self.registry {
            if let Some(itemref) = registry.find_itemref(name) {
                if let Some(info) = registry.get_info(&itemref) {
                    Ok(info.iter())
                } else {
                    Ok(Box::new(Vec::new().into_iter()))
                }
            } else {
                Err(format!("No item found with name {name}"))
            }
        } else {
            Err(format!("Currently no configuration loaded"))
        }
    }

    /// Retrieves the mutation start of a given (menu) configuration item
    pub fn mutation_start(&self, name: &str) -> Result<MutationStart, String> {
        if let Some(registry) = &self.registry {
            if let Some(itemref) = registry.find_itemref(name) {
                match itemref {
                    ItemReference::Config(_) | ItemReference::MenuConfig(_) => {
                        match registry.mutation_start(&itemref) {
                            Some(mutation_start) => Ok(mutation_start),
                            None => Err(format!("Item {name} can not currently be changed")),
                        }
                    }
                    _ => Err(format!("Item {name} refers to a menu, can not be set")),
                }
            } else {
                Err(format!("No item found with name {name}"))
            }
        } else {
            Err(format!("Currently no configuration loaded"))
        }
    }

    /// Sets the value of the given (menu) configuration item
    pub fn set_value(&mut self, name: &str, value: Variant) -> Result<(), String> {
        if let Some(registry) = &mut self.registry {
            if let Some(itemref) = registry.find_itemref(name) {
                match itemref {
                    ItemReference::Config(_) | ItemReference::MenuConfig(_) => {
                        if let Err(e) = registry.set_value(&itemref, value) {
                            return Err(format!("{e}"));
                        } else {
                            Ok(())
                        }
                    }
                    _ => Err(format!("Item {name} refers to a menu, can not be set")),
                }
            } else {
                Err(format!("No item found with name {name}"))
            }
        } else {
            Err(format!("Currently no configuration loaded"))
        }
    }

    /// Lists the subitems of the currently activated menu item or menu
    /// configurtion item and returns them as item references
    pub fn list_itemrefs(&self) -> Vec<ItemReference> {
        let mut result = Vec::new();
        if let Some(registry) = &self.registry {
            if let Some(itemref) = self.active_item() {
                // finds all itemreferences which depend on item.
                for (subitemref, _) in registry.items() {
                    if registry.item_enabled(subitemref).unwrap_or(false) {
                        if let Some(parents) = self.current_parents(subitemref) {
                            for parent in parents {
                                if parent == itemref {
                                    result.push(subitemref.clone());
                                }
                            }
                        }
                    }
                }
            }
        }
        result
    }

    /// Get the current (enabled) parent(s) referenced by an item
    pub fn current_parents(&self, itemref: &ItemReference) -> Option<Vec<ItemReference>> {
        match &self.registry {
            Some(registry) => {
                registry.get(itemref).map({
                    |item| {
                        let mut current_weight = 4;
                        // The root menu is the default parent, unless the itemreference
                        // is the root menu
                        let root_ref = registry.main_menu_item().itemref();
                        let mut found = if &root_ref == itemref {
                            vec![]
                        } else {
                            vec![registry.main_menu_item().itemref()]
                        };
                        for item_dependency in item.item_dependencies() {
                            // if an item dependency is a simple
                            // reference to an ItemReference it counts,
                            // otherwise it is to be skipped.
                            match item_dependency {
                                ItemDependency::Heavy(expr) => {
                                    if current_weight >= 1 {
                                        let item_expr = expr.item_expr();
                                        if let ItemExpr::ItemReference(other) = item_expr {
                                            if registry.item_enabled(other).unwrap_or(false) {
                                                if current_weight > 1 {
                                                    current_weight = 1;
                                                    found = vec![other.clone()];
                                                } else if current_weight == 1 {
                                                    found.push(other.clone());
                                                }
                                            }
                                        };
                                    }
                                }
                                ItemDependency::Normal(expr) => {
                                    if current_weight >= 2 {
                                        let item_expr = expr.item_expr();
                                        if let ItemExpr::ItemReference(other) = item_expr {
                                            if registry.item_enabled(other).unwrap_or(false) {
                                                if current_weight > 2 {
                                                    current_weight = 2;
                                                    found = vec![other.clone()];
                                                } else if current_weight == 2 {
                                                    found.push(other.clone());
                                                }
                                            }
                                        };
                                    }
                                }
                                ItemDependency::Trivial(expr) => {
                                    if current_weight >= 3 {
                                        let item_expr = expr.item_expr();
                                        if let ItemExpr::ItemReference(other) = item_expr {
                                            if registry.item_enabled(other).unwrap_or(false) {
                                                if current_weight > 3 {
                                                    current_weight = 3;
                                                    found = vec![other.clone()];
                                                } else if current_weight == 3 {
                                                    found.push(other.clone());
                                                }
                                            }
                                        };
                                    }
                                }
                            }
                        }

                        found
                    }
                })
            }
            None => None,
        }
    }

    /// Gets the currently activated menu item or menu configuration item
    pub fn active_item(&self) -> Option<ItemReference> {
        match &self.registry {
            Some(registry) => {
                let mut activated = registry.main_menu_item().itemref();
                for itemref in &self.activated_menus {
                    if !registry.item_enabled(&itemref).unwrap_or(false) {
                        break;
                    } else {
                        activated = itemref.clone();
                    }
                }
                Some(activated)
            }
            None => None,
        }
    }

    /// Gets the names of the currently activated menu items
    pub fn active_menu_list(&self) -> Vec<String> {
        let mut result: Vec<ItemReference> = vec![];
        if let Some(registry) = &self.registry {
            let menus = self.activated_menus.clone();
            let root_menu = registry.main_menu_item().itemref();
            result.push(root_menu);
            result.extend(menus);
        }
        result.iter().map(|itemref| itemref.to_string()).collect()
    }

    /// Changes the current active item by either pushing or popping a menu item,
    /// given the navigation instruction.
    pub fn ch_menu(&mut self, navigation: Navigation) -> Result<(), String> {
        match &navigation {
            Navigation::Up => {
                if !self.activated_menus.is_empty() {
                    self.activated_menus.pop();
                    Ok(())
                } else {
                    Err(format!("Can not navigate up anymore"))
                }
            }
            Navigation::Down(name) => {
                if let Some(registry) = &self.registry {
                    let itemref = match registry.find_itemref(name) {
                        Some(itemref) => itemref,
                        None => {
                            return Err(format!(
                                "Could not find menu or menu configuration with name {name}"
                            ));
                        }
                    };
                    match itemref {
                        ItemReference::Menu(_) | ItemReference::MenuConfig(_) => {
                            self.activated_menus.push(itemref);
                            Ok(())
                        }
                        ItemReference::Config(_) => Err(format!(
                            "Could not find menu or menu configuration with name {name}"
                        )),
                    }
                } else {
                    Err(format!("No configuration loaded"))
                }
            }
        }
    }
}

/// Returns the information after (successfully) loading a Kconfig file
pub struct SessionLoadInfo {
    /// The load error, where applicable, contains the errors of load issues
    /// when attempting to overlay the .config over de loaded Kconfig file, when
    /// applicable
    pub maybe_load_error: Option<LoadError>,

    /// Returns output rendered from for example macros which emit information
    pub load_transcript: Vec<(bool, String)>,
}

#[derive(Clone)]
struct ConsoleOutput {
    ref_vec: Rc<RefCell<Vec<(bool, String)>>>,
}

impl ConsoleOutput {
    fn new() -> Self {
        Self {
            ref_vec: Rc::new(RefCell::new(Vec::new())),
        }
    }

    fn transcript(&self) -> Vec<(bool, String)> {
        self.ref_vec.borrow().clone()
    }
}

impl FunctionConsoleOutput for ConsoleOutput {
    fn writeln(&self, s: &str) {
        self.ref_vec.borrow_mut().push((false, s.to_string()))
    }

    fn ewriteln(&self, s: &str) {
        self.ref_vec.borrow_mut().push((true, s.to_string()))
    }
}

impl Default for Session {
    /// Returns a default implementation of Self, pointing to the Kconfig
    /// and .config files in the current working directory.
    fn default() -> Self {
        let kconfig = PathBuf::from("Kconfig");
        let dotconfig = PathBuf::from(".config");
        let end_of_text_input = String::new();
        let changed = false;
        let registry = None;
        let activated_menus = vec![];
        Session {
            kconfig,
            dotconfig,
            end_of_text_input,
            changed,
            registry,
            activated_menus,
        }
    }
}

pub struct PrintableListItem {
    itemtype: ListItemType,
    name: String,
    value: Option<Variant>,
    is_default: bool,
}

impl Display for PrintableListItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self.itemtype {
            ListItemType::Config | ListItemType::MenuConfig => {
                let value = self.value.clone().unwrap_or(Variant::from(false));
                let formatted_value = if value.datatype() == VariantDataType::String {
                    escape_string(&format!("{value}"))
                } else {
                    format!("{value}")
                };
                write!(
                    f,
                    "[{}] {}: {}",
                    if self.is_default { ' ' } else { '*' },
                    self.name,
                    formatted_value
                )
            }
            ListItemType::Menu => {
                write!(f, "{} -->", self.name)
            }
        }
    }
}

pub enum ListItemType {
    Config,
    MenuConfig,
    Menu,
}

#[cfg(test)]
mod tests {
    use crate::client::{Navigation, Routes};

    use super::ConfigSetting;

    #[test]
    fn test_routes() {
        assert!(Routes::ConfigGet(None) == Routes::ConfigGet(None));
        assert!(
            Routes::ConfigGet(Some(ConfigSetting::Input))
                == Routes::ConfigGet(Some(ConfigSetting::Input))
        );
        assert!(Routes::ConfigSet(ConfigSetting::Input) == Routes::ConfigSet(ConfigSetting::Input));
        assert!(Routes::Save == Routes::Save);
        assert!(Routes::Load(true) == Routes::Load(true));
        assert!(Routes::Load(false) == Routes::Load(false));
        assert!(Routes::Unload(true) == Routes::Unload(true));
        assert!(Routes::Unload(false) == Routes::Unload(false));
        assert!(Routes::ChMenu(Navigation::Up) == Routes::ChMenu(Navigation::Up));
        assert!(
            Routes::ChMenu(Navigation::Down("FOO".to_owned()))
                == Routes::ChMenu(Navigation::Down("FOO".to_owned()))
        );
        assert!(Routes::Info("FOO".to_owned()) == Routes::Info("FOO".to_owned()));
        assert!(Routes::Update("FOO".to_owned()) == Routes::Update("FOO".to_owned()));
        assert!(Routes::Exit(true) == Routes::Exit(true));
        assert!(Routes::Exit(false) == Routes::Exit(false));
    }

    #[test]
    fn test_navigation() {
        assert!(Navigation::Up == Navigation::Up);
        assert!(Navigation::Down("FOO".to_owned()) == Navigation::Down("FOO".to_owned()));
        assert!(Navigation::Up != Navigation::Down("FOO".to_owned()));
        assert!(Navigation::Down("FOO".to_owned()) != Navigation::Up);
        assert!(Navigation::Down("FOO".to_owned()) != Navigation::Down("BAR".to_owned()));
    }

    #[test]
    fn test_config_settings() {
        assert!(ConfigSetting::Input == ConfigSetting::Input);
        assert!(ConfigSetting::Output == ConfigSetting::Output);
        assert!(ConfigSetting::EndOfTextInput == ConfigSetting::EndOfTextInput);
        assert!(ConfigSetting::Input != ConfigSetting::Output);
        assert!(ConfigSetting::Output != ConfigSetting::EndOfTextInput);
        assert!(ConfigSetting::EndOfTextInput != ConfigSetting::Input);
        assert!(ConfigSetting::Input != ConfigSetting::EndOfTextInput);
        assert!(ConfigSetting::Output != ConfigSetting::Input);
        assert!(ConfigSetting::EndOfTextInput != ConfigSetting::Output);
    }

    #[test]
    fn test_config_settings_display() {
        assert_eq!("input", format!("{}", ConfigSetting::Input));
        assert_eq!("output", format!("{}", ConfigSetting::Output));
        assert_eq!(
            "end-of-text-input",
            format!("{}", ConfigSetting::EndOfTextInput)
        );
    }
}
