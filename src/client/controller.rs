/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This module contains the controller of the client, allowing requests
//! from the presenter to be fulfilled.

use crate::client::{ConfigSetting, Navigation, Routes, Session};
use kconfig_represent::{MutationStart, Tristate, Variant, VariantDataType};
use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;

pub struct Controller<PLn, EPLn, RLn, PSP>
where
    PLn: Fn(&str),
    EPLn: Fn(&str),
    RLn: Fn() -> String,
    PSP: Fn(),
{
    ref_session: Rc<RefCell<Session>>,
    println: PLn,
    eprintln: EPLn,
    readln: RLn,
    print_secondary_prompt: PSP,
}

impl<PLn, EPLn, RLn, PSP> Controller<PLn, EPLn, RLn, PSP>
where
    PLn: Fn(&str),
    EPLn: Fn(&str),
    RLn: Fn() -> String,
    PSP: Fn(),
{
    pub fn new(println: PLn, eprintln: EPLn, readln: RLn, print_secondary_prompt: PSP) -> Self {
        Self {
            ref_session: Rc::new(RefCell::new(Session::default())),
            println,
            eprintln,
            readln,
            print_secondary_prompt,
        }
    }

    pub fn route(&self, route: Routes) -> ContinueOrStop {
        match route {
            Routes::ConfigGet(maybe_config_setting) => self.config_get(maybe_config_setting),
            Routes::ConfigSet(config_setting) => self.config_set(&config_setting),
            Routes::Save => {
                self.save(true);
            }
            Routes::Load(no_prompt) => self.load(no_prompt),
            Routes::Unload(no_prompt) => self.unload(no_prompt),
            Routes::Exit(no_prompt) => return self.exit(no_prompt),
            Routes::List => self.list(),
            Routes::ChMenu(navigation) => self.ch_menu(navigation),
            Routes::Update(name) => self.update(&name),
            Routes::Info(name) => self.info(&name),
        }

        ContinueOrStop::Continue
    }

    pub fn active_menu_list(&self) -> Vec<String> {
        self.ref_session.borrow().active_menu_list()
    }

    fn config_get(&self, maybe_config_setting: Option<ConfigSetting>) {
        match &maybe_config_setting {
            Some(config_setting) => {
                self.println(&format!(
                    "{}",
                    self.ref_session.borrow().setting(config_setting)
                ));
            }
            None => {
                for config_setting in vec![
                    ConfigSetting::Input,
                    ConfigSetting::Output,
                    ConfigSetting::EndOfTextInput,
                ] {
                    self.println(&format!(
                        "{config_setting}: {}",
                        self.ref_session.borrow().setting(&config_setting)
                    ));
                }
            }
        }
    }

    fn config_set(&self, config_setting: &ConfigSetting) {
        let line = self.read_line();
        if !line.is_empty() || config_setting == &ConfigSetting::EndOfTextInput {
            match config_setting {
                ConfigSetting::Input => {
                    let path = PathBuf::from(line);
                    self.ref_session.borrow_mut().set_kconfig(&path);
                }
                ConfigSetting::Output => {
                    let path = PathBuf::from(line);
                    self.ref_session.borrow_mut().set_dotconfig(&path);
                }
                ConfigSetting::EndOfTextInput => {
                    self.ref_session.borrow_mut().set_end_of_text_input(&line);
                }
            }
        } else {
            self.eprintln(&format!(
                "Configuration setting '{config_setting}' not set, no value provided"
            ));
        }
    }

    fn save(&self, no_prompt: bool) -> bool {
        if !self.loaded() {
            self.eprintln(&format!(
                "No configuration session active, nothing to be saved"
            ));
            return true;
        }
        if !no_prompt {
            let mut line = "".to_owned();
            while line != "yes".to_owned() && line != "no".to_owned() && line != "cancel".to_owned()
            {
                self.println(&format!("Do you want to save the current configuration?"));
                self.println(&format!("Enter yes / no / cancel"));
                line = self.read_line();
                if line == "no".to_owned() {
                    return true;
                } else if line == "cancel".to_owned() {
                    return false;
                }
            }
        }
        self.ref_session.borrow_mut().save();
        self.println(&format!("Configuration saved"));
        true
    }

    fn loaded(&self) -> bool {
        self.ref_session.borrow().loaded()
    }

    fn load(&self, no_prompt: bool) {
        {
            let session = self.ref_session.borrow();
            while session.changed() && !no_prompt {
                if !self.save(false) {
                    // abort if the save action has been cancelled
                    return;
                }
            }
        }
        self.ref_session.borrow_mut().unload();
        match self.ref_session.borrow_mut().load() {
            Ok(load_info) => {
                for (is_err, output) in load_info.load_transcript {
                    if is_err {
                        self.eprintln(&output);
                    } else {
                        self.println(&output);
                    }
                }

                if let Some(load_error) = load_info.maybe_load_error {
                    self.eprintln(&format!("{load_error}"));
                }
            }
            Err(s) => self.eprintln(&s),
        }
    }

    fn unload(&self, no_prompt: bool) {
        {
            let session = self.ref_session.borrow();
            while session.changed() && !no_prompt {
                if !self.save(false) {
                    // abort if the save action has been cancelled
                    return;
                }
            }
        }
        self.ref_session.borrow_mut().unload();
    }

    fn list(&self) {
        match self.ref_session.borrow().list() {
            Ok(line_items) => {
                for line_item in line_items {
                    self.println(&format!("{line_item}"));
                }
            }
            Err(s) => self.eprintln(&s),
        }
    }

    fn ch_menu(&self, navigation: Navigation) {
        if let Err(s) = self.ref_session.borrow_mut().ch_menu(navigation) {
            self.eprintln(&s)
        }
    }

    fn update(&self, name: &str) {
        let mutation_start = { self.ref_session.borrow().mutation_start(name) };
        match mutation_start {
            Ok(mutation_start) => {
                match mutation_start {
                    MutationStart::Bool(v) => {
                        // Flip the value
                        self.println(&format!(
                            "Switching value of item {name} from {} to {}",
                            Variant::from(v),
                            Variant::from(!v)
                        ));
                        if let Err(s) = self
                            .ref_session
                            .borrow_mut()
                            .set_value(name, Variant::from(!v))
                        {
                            self.eprintln(&s);
                            return;
                        }
                        self.ref_session.borrow_mut().flag_changed();
                    }
                    MutationStart::Tristate(v) => {
                        // Rotate the value, false > maybe > true
                        let tristate: Tristate = v.into();
                        let new_tristate = match tristate {
                            Tristate::FALSE => Tristate::MAYBE,
                            Tristate::MAYBE => Tristate::TRUE,
                            _ => Tristate::FALSE,
                        };
                        self.println(&format!(
                            "Switching value of item {name} from {} to {}",
                            Variant::from(tristate),
                            Variant::from(new_tristate)
                        ));
                        if let Err(s) = self
                            .ref_session
                            .borrow_mut()
                            .set_value(name, Variant::from(new_tristate))
                        {
                            self.eprintln(&s);
                            return;
                        }
                        self.ref_session.borrow_mut().flag_changed();
                    }
                    MutationStart::Int(v) => {
                        self.println(&format!("⮚ Original value of item {name}: {v}"));
                        self.println(&format!("⮚ Please enter new value:"));
                        let value = Variant::parse(&self.read_line());
                        if let VariantDataType::Int = value.datatype() {
                            self.println(&format!("Switching value of item {name} to {value}"));
                            if let Err(s) = self
                                .ref_session
                                .borrow_mut()
                                .set_value(name, Variant::from(value))
                            {
                                self.eprintln(&s);
                                return;
                            }
                            self.ref_session.borrow_mut().flag_changed();
                        } else {
                            self.eprintln(&format!(
                                "The given value {value} is an invalid sequence for item {name}"
                            ));
                        }
                    }
                    MutationStart::Hex(v) => {
                        self.println(&format!("⮚ Original value of item {name}: {v}"));
                        self.println(&format!("⮚ Please enter new value:"));
                        let value = Variant::parse(&self.read_line());
                        if let VariantDataType::Hex = value.datatype() {
                            self.println(&format!("Switching value of item {name} to {value}"));
                            if let Err(s) = self
                                .ref_session
                                .borrow_mut()
                                .set_value(name, Variant::from(value))
                            {
                                self.eprintln(&s);
                                return;
                            }
                            self.ref_session.borrow_mut().flag_changed();
                        } else {
                            self.eprintln(&format!(
                                "The given value {value} is an invalid sequence for item {name}"
                            ));
                        }
                    }
                    MutationStart::String(v) => {
                        self.println(&format!("⮚ Original value of item {name}:\n\n{v}\n"));
                        let end_of_text_input = self.get_end_of_text_input();
                        if end_of_text_input.is_empty() {
                            self.println(&format!(
                            "⮚ Please enter new value, end the sequence with two empty newlines:\n"
                        ));
                        } else {
                            self.println(&format!(
                            "⮚ Please enter new value, end the sequence with a line containing exactly: \n{end_of_text_input}\n"
                        ));
                        }
                        let mut value = String::new();
                        let mut first_entry = true;
                        loop {
                            let line = self.read_line_smart_trim();
                            if line != end_of_text_input {
                                if !first_entry {
                                    value.push('\n');
                                } else {
                                    first_entry = false;
                                }
                                value.push_str(&line);
                            } else {
                                break;
                            }
                        }
                        self.eprintln(&format!("Setting new value of item {name}"));
                        if let Err(s) = self
                            .ref_session
                            .borrow_mut()
                            .set_value(name, Variant::from(value))
                        {
                            self.eprintln(&s);
                            return;
                        }
                        self.ref_session.borrow_mut().flag_changed();
                    }
                    MutationStart::None => {
                        self.eprintln(&format!("Value of item {name} can not be set"));
                    }
                }
            }
            Err(s) => self.eprintln(&s),
        }
    }

    fn info(&self, name: &str) {
        match self.ref_session.borrow().info(name) {
            Ok(info) => {
                let mut has_info = false;
                for line in info {
                    self.println(&format!("{line}"));
                    has_info = true;
                }
                if !has_info {
                    self.println(&format!("⮚ No help section found in item {name}"));
                } else {
                    self.println("");
                }
            }
            Err(s) => self.eprintln(&s),
        }
    }

    fn exit(&self, no_prompt: bool) -> ContinueOrStop {
        if self.ref_session.borrow().changed() && !no_prompt {
            if !self.save(false) {
                // abort if the save action has been cancelled
                return ContinueOrStop::Continue;
            }
        }
        ContinueOrStop::Stop
    }

    fn println(&self, s: &str) {
        (self.println)(s);
    }

    fn eprintln(&self, s: &str) {
        (self.eprintln)(s);
    }

    fn print_secondary_prompt(&self) {
        (self.print_secondary_prompt)();
    }

    fn get_end_of_text_input(&self) -> String {
        let session = self.ref_session.borrow();
        let eot = session.end_of_text_input();
        eot
    }

    fn read_line(&self) -> String {
        self.print_secondary_prompt();
        (self.readln)().trim().to_string()
    }

    fn read_line_smart_trim(&self) -> String {
        self.print_secondary_prompt();
        let line = (self.readln)();
        let mut chars: Vec<char> = line.chars().collect();

        if chars.len() > 1
            && chars.get(chars.len() - 2) == Some(&'\n')
            && chars.get(chars.len() - 1) == Some(&'\r')
        {
            chars.pop();
            chars.pop();
            chars.iter().cloned().collect::<String>()
        } else if chars.len() > 1
            && chars.get(chars.len() - 2) == Some(&'\r')
            && chars.get(chars.len() - 1) == Some(&'\n')
        {
            chars.pop();
            chars.pop();
            chars.iter().cloned().collect::<String>()
        } else if chars.len() > 0 && chars.get(chars.len() - 1) == Some(&'\n') {
            chars.pop();
            chars.iter().cloned().collect::<String>()
        } else {
            line
        }
    }
}

pub enum ContinueOrStop {
    Continue,
    Stop,
}
