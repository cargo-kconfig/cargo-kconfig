use std::fmt::Display;

/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/// Specifies if help is requested, what kind of help is requested.
#[derive(Debug, PartialEq, Eq)]
pub enum HelpType {
    /// Print a generic help text if no exact command is defined, or if an
    /// erroneous command is used.
    None,
    /// Print the help of the "set" command
    ConfigSet,
    /// Print the help of the "get" command
    ConfigGet,
    /// Print the help of the "save" command
    Save,
    /// Prints the help of the "load" command
    Load,
    /// Prints the help of the "unload" command
    Unload,
    /// Print the help of the "list" or "ls" command
    List,
    /// Changes the current active item
    ChMenu,
    /// Prints the help of the "info" command
    Info,
    /// Prints the help of the "update" command
    Update,
    /// Prints the help of the "exit" command
    Exit,
}

impl HelpType {
    fn print_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: <subcommand> {{...}}")?;
        writeln!(f, "          <subcommand> --help")?;
        writeln!(f, "")?;
        writeln!(f, "Cargo Kconfig supports the following subcommands:")?;
        writeln!(f, "")?;

        write_header(f, "Application Behavior")?;
        writeln!(f, "help        Shows this help text")?;
        writeln!(f, "config-set  Sets cargo-kconfig configuration values")?;
        writeln!(f, "config-get  Gets cargo-kconfig configuration values")?;
        writeln!(f, "exit        Exits the Read-Eval-Print-Loop")?;

        write_header(f, "File Behavior")?;
        writeln!(f, "save        Saves the configuration, where applicable")?;
        writeln!(f, "load        Loads the configuration")?;
        writeln!(f, "unload      Unloads the configuration")?;

        write_header(f, "Inspection")?;
        writeln!(f, "ls | list   Shows the items of the active menu")?;
        writeln!(f, "info        Shows the explanation of the desired item")?;

        write_header(f, "Modification")?;
        writeln!(f, "update      Updates a configuration item to a new value")?;
        Ok(())
    }

    fn print_config_set_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: config-set input")?;
        writeln!(f, "          config-set output")?;
        writeln!(f, "          config-set end-of-text-input")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This subcommand sets the settings necessary to load and write the ",
                "Kconfig configuration template and the .config configuration file. ",
                "If attempting to set the path to the input file while it has already ",
                "been opened, the currently loaded configuration template will be closed"
            )
        )?;
        writeln!(f, "")?;
        writeln!(
            f,
            "input - Attempt to set the path of an (alternative) Kconfig configuration file to use"
        )?;
        writeln!(
            f,
            "output - Attempt to set the path of an (alternative) .config output file to use"
        )?;
        writeln!(
            f,
            "end-of-text-input - Sets the string used when retrieving a string configuration item indicating the end of input"
        )?;
        Ok(())
    }

    fn print_config_get_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: config-get list")?;
        writeln!(f, "          config-get input")?;
        writeln!(f, "          config-get output")?;
        writeln!(f, "          config-get end-of-text-input")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This subcommand gets the settings necessary to load and write the ",
                "Kconfig configuration template and the .config configuration file. ",
            )
        )?;
        writeln!(f, "")?;
        writeln!(
            f,
            "input - Attempt to get the path of an (alternative) Kconfig configuration file to use"
        )?;
        writeln!(
            f,
            "output - Attempt to get the path of an (alternative) .config output file to use"
        )?;
        writeln!(
            f,
            "end-of-text-input - Gets the string used when retrieving a string configuration item indicating the end of input"
        )?;
        writeln!(f, "list - Attempt to list all the known settings")?;
        Ok(())
    }

    fn print_save_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: save")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This command saves the current configuration to the .config file or when ",
                "applicable the alternative configured output file"
            )
        )?;
        Ok(())
    }

    fn print_load_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: load")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This command loads the configuration from the Kconfig file, and loads the stored ",
                "configuration from the .config or when applicable the alternative configured output file ",
                "when present on the system"
            )
        )?;
        Ok(())
    }

    fn print_unload_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: load [--noprompt] [--help]")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This command unloads the already loaded configuration If the configuration has ",
                "not been saved, requests to save the configuration.\n\n",
                "If --noprompt is set, then the request to save the configuation ",
                "will not be shown, and no automatic save will occur\n\n",
                "If --help is set, then this message will be displayed\n"
            )
        )?;
        Ok(())
    }

    fn print_list_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: list")?;
        writeln!(f, "          ls")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This command lists the subitems of the current activated menu item or",
                "menu configuration item"
            )
        )?;
        Ok(())
    }

    fn print_ch_menu_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: chmenu {{NAME}}")?;
        writeln!(f, "👉 Usage: chmenu ..")?;
        writeln!(f, "")?;
        writeln!(f, "NAME    - The name of a configuration item")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This command changes the current active menu item or menu configuration item",
                "either by attempting to navigate downwards (using the name) or upwards (using ..)"
            )
        )?;
        Ok(())
    }

    fn print_info_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: info {{NAME}}")?;
        writeln!(f, "")?;
        writeln!(f, "NAME    - The name of a configuration item")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "This command shows the help text containing further explanation about the given item."
            )
        )?;
        Ok(())
    }

    fn print_update_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: update {{NAME}}")?;
        writeln!(f, "")?;
        writeln!(f, "NAME    - The name of a configuration item")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!("This command changes the value of the given (menu) configuration item.")
        )?;
        Ok(())
    }

    fn print_exit_help(f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "👉 Usage: exit [--noprompt]")?;
        writeln!(f, "")?;
        writeln!(
            f,
            concat!(
                "Exits the Read-Eval-Print-Loop. If the configuration has ",
                "not been saved, requests to save the configuration.\n\n",
                "If --noprompt is set, then the request to save the configuation ",
                "will not be shown, and no automatic save will occur\n\n",
                "If --help is set, then this message will be displayed\n"
            )
        )?;
        Ok(())
    }
}

fn write_header(f: &mut std::fmt::Formatter<'_>, s: &str) -> std::fmt::Result {
    write!(f, "\n⮚ {s} ⮘\n")
}

impl Display for HelpType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            HelpType::None => Self::print_help(f),
            HelpType::ConfigSet => Self::print_config_set_help(f),
            HelpType::ConfigGet => Self::print_config_get_help(f),
            HelpType::Save => Self::print_save_help(f),
            HelpType::Load => Self::print_load_help(f),
            HelpType::Unload => Self::print_unload_help(f),
            HelpType::List => Self::print_list_help(f),
            HelpType::ChMenu => Self::print_ch_menu_help(f),
            HelpType::Info => Self::print_info_help(f),
            HelpType::Update => Self::print_update_help(f),
            HelpType::Exit => Self::print_exit_help(f),
        }
    }
}
