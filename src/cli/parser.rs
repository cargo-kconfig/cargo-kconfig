/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use crate::client::Navigation;
use crate::client::{ConfigSetting, Routes};
use std::collections::VecDeque;

use super::HelpType;

/// Specifies the options given to the command line parser. This is used to
/// determine which strategy to use for starting a client, a server or displaying
/// help text.
#[derive(Debug, PartialEq, Eq)]
pub enum CliOpts {
    /// If a client is to be started, contains the options for a client
    Command(Routes),

    /// If help is requests, contains what kind of help is requested. If None
    /// is set, generic help is requested, explaining what options are available.
    Help(ParseError, HelpType),
}

/// A ParseError is set as part of the Help CliOpts enumerator item to
/// indicate why the help should be printed, and what exit code after printing
/// the help should be returned to the invoking process.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ParseError(i8);

/// Indicates no parsing error has occurred
pub const NO_PARSE_ERROR: ParseError = ParseError(0);

/// Indicates a generic parse error has occurred
pub const PARSE_ERROR: ParseError = ParseError(1);

impl CliOpts {
    /// Parses the command line arguments into a CliOpts struct. If parsing
    /// was erroneous, shows an error, and returns the closest possible Help
    /// text for the user to determine which error was made.
    pub fn parse(input: &str) -> Option<CliOpts> {
        // Check if the input is actually an empty input
        if input.trim().is_empty() {
            return None;
        }

        let mut args: VecDeque<&str> = input.split(|c: char| c.is_whitespace()).collect();

        // The first argument contains the type of command to expect, this
        // can either be client or server. If no command is specified, or an
        // invalid command is specified, help should be displayed. Also,
        // if an invalid command is specified, an error should be displayed
        // as such.
        let cmdname = args.pop_front().unwrap();

        Some(if cmdname == "help" {
            Self::Help(NO_PARSE_ERROR, HelpType::None)
        } else {
            dispatcher::dispatch(&cmdname, &Vec::from(args))
        })
    }

    fn parse_exit(args: &Vec<&str>) -> CliOpts {
        let mut is_noprompt = false;

        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::Exit);
            } else if v == &"--noprompt" && !is_noprompt {
                is_noprompt = true;
            } else {
                return CliOpts::Help(PARSE_ERROR, HelpType::Exit);
            }
        }

        CliOpts::Command(Routes::Exit(is_noprompt))
    }

    fn parse_config_get(args: &Vec<&str>) -> CliOpts {
        let mut args_done = false;
        let mut result = None;
        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::ConfigGet);
            }
            if args_done == true {
                return CliOpts::Help(PARSE_ERROR, HelpType::ConfigGet);
            }
            args_done = true;
            if v == &"input" {
                result = Some(Routes::ConfigGet(Some(ConfigSetting::Input)));
            }
            if v == &"output" {
                result = Some(Routes::ConfigGet(Some(ConfigSetting::Output)));
            }
            if v == &"end-of-text-input" {
                result = Some(Routes::ConfigGet(Some(ConfigSetting::EndOfTextInput)));
            }
            if v == &"list" {
                result = Some(Routes::ConfigGet(None));
            }
        }
        match result {
            Some(routes) => return CliOpts::Command(routes),
            None => return CliOpts::Help(PARSE_ERROR, HelpType::ConfigGet),
        }
    }

    fn parse_config_set(args: &Vec<&str>) -> CliOpts {
        let mut args_done = false;
        let mut result = None;
        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::ConfigSet);
            }
            if args_done == true {
                return CliOpts::Help(PARSE_ERROR, HelpType::ConfigSet);
            }
            args_done = true;
            if v == &"input" {
                result = Some(Routes::ConfigSet(ConfigSetting::Input));
            }
            if v == &"output" {
                result = Some(Routes::ConfigSet(ConfigSetting::Output));
            }
            if v == &"end-of-text-input" {
                result = Some(Routes::ConfigSet(ConfigSetting::EndOfTextInput));
            }
        }
        match result {
            Some(routes) => CliOpts::Command(routes),
            None => CliOpts::Help(PARSE_ERROR, HelpType::ConfigSet),
        }
    }

    fn parse_save(args: &Vec<&str>) -> CliOpts {
        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::Save);
            }
            return CliOpts::Help(PARSE_ERROR, HelpType::Save);
        }
        CliOpts::Command(Routes::Save)
    }

    fn parse_load(args: &Vec<&str>) -> CliOpts {
        let mut is_noprompt = false;

        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::Load);
            } else if v == &"--noprompt" && !is_noprompt {
                is_noprompt = true;
            } else {
                return CliOpts::Help(PARSE_ERROR, HelpType::Load);
            }
        }

        CliOpts::Command(Routes::Load(is_noprompt))
    }

    fn parse_unload(args: &Vec<&str>) -> CliOpts {
        let mut is_noprompt = false;

        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::Unload);
            } else if v == &"--noprompt" && !is_noprompt {
                is_noprompt = true;
            } else {
                return CliOpts::Help(PARSE_ERROR, HelpType::Unload);
            }
        }

        CliOpts::Command(Routes::Unload(is_noprompt))
    }

    fn parse_list(args: &Vec<&str>) -> CliOpts {
        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::List);
            } else {
                return CliOpts::Help(PARSE_ERROR, HelpType::List);
            }
        }
        return CliOpts::Command(Routes::List);
    }

    fn parse_ch_menu(args: &Vec<&str>) -> CliOpts {
        // Goes into a child, if an argument is set (with the exception of --help),
        // navigates up if the argument .. is used
        let mut menu_name = String::new();

        for v in args {
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::ChMenu);
            }
            if !menu_name.is_empty() {
                menu_name.push(' ');
            }
            menu_name.push_str(v);
        }

        match menu_name.is_empty() {
            true => CliOpts::Help(PARSE_ERROR, HelpType::ChMenu),
            false => CliOpts::Command(if menu_name.eq("..") {
                Routes::ChMenu(Navigation::Up)
            } else {
                Routes::ChMenu(Navigation::Down(menu_name))
            }),
        }
    }

    fn parse_update(args: &Vec<&str>) -> CliOpts {
        let mut args_done = false;
        let mut result = None;
        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::Update);
            }
            if args_done == true {
                return CliOpts::Help(PARSE_ERROR, HelpType::Update);
            }
            args_done = true;
            result = Some(Routes::Update(v.to_string()));
        }
        match result {
            Some(routes) => CliOpts::Command(routes),
            None => CliOpts::Help(PARSE_ERROR, HelpType::Update),
        }
    }

    fn parse_info(args: &Vec<&str>) -> CliOpts {
        let mut args_done = false;
        let mut result = None;
        for v in args {
            if v == &"" {
                continue;
            }
            if v == &"--help" {
                return CliOpts::Help(NO_PARSE_ERROR, HelpType::Info);
            }
            if args_done == true {
                return CliOpts::Help(PARSE_ERROR, HelpType::Info);
            }
            args_done = true;
            result = Some(Routes::Info(v.to_string()));
        }
        match result {
            Some(routes) => CliOpts::Command(routes),
            None => CliOpts::Help(PARSE_ERROR, HelpType::Info),
        }
    }
}

mod dispatcher {
    use std::{collections::HashMap, sync::Once};

    use crate::cli::{HelpType, PARSE_ERROR};

    use super::CliOpts;

    #[derive(Clone)]
    struct Dispatched {
        callback: fn(&Vec<&str>) -> CliOpts,
    }

    /// Dispatches a given command to the appropriate helper, defined
    /// by the command name.
    pub(super) fn dispatch(cmdname: &str, args: &Vec<&str>) -> CliOpts {
        match get_subcommands().get(cmdname) {
            Some(dispatched) => (dispatched.callback)(args),
            None => CliOpts::Help(PARSE_ERROR, HelpType::None),
        }
    }

    static mut SUBCOMMANDS: Option<HashMap<String, Dispatched>> = None;
    static START: Once = Once::new();

    fn get_subcommands() -> &'static HashMap<String, Dispatched> {
        START.call_once(|| unsafe {
            let mut map = HashMap::new();
            // Configuration of the menu application itself
            add_command(&mut map, "config-get", super::CliOpts::parse_config_get);
            add_command(&mut map, "config-set", super::CliOpts::parse_config_set);
            // File commands
            add_command(&mut map, "exit", super::CliOpts::parse_exit);
            add_command(&mut map, "save", super::CliOpts::parse_save);
            add_command(&mut map, "load", super::CliOpts::parse_load);
            add_command(&mut map, "unload", super::CliOpts::parse_unload);
            // Navigation commands
            add_command(&mut map, "chmenu", super::CliOpts::parse_ch_menu);
            // Inspection commands
            add_command(&mut map, "list", super::CliOpts::parse_list);
            add_command(&mut map, "ls", super::CliOpts::parse_list);
            add_command(&mut map, "info", super::CliOpts::parse_info);
            // Modification commands
            add_command(&mut map, "update", super::CliOpts::parse_update);
            SUBCOMMANDS = Some(map);
        });

        unsafe {
            match &SUBCOMMANDS {
                Some(subcommands) => subcommands,
                None => panic!("⛔ Subcommand-map not initialized"),
            }
        }
    }

    fn add_command(
        map: &mut HashMap<String, Dispatched>,
        name: &str,
        callback: fn(&Vec<&str>) -> CliOpts,
    ) {
        map.insert(name.to_owned(), Dispatched { callback });
    }
}

#[cfg(test)]
mod test {
    use crate::{
        cli::{HelpType, NO_PARSE_ERROR, PARSE_ERROR},
        client::Routes,
        client::{ConfigSetting, Navigation},
    };

    use super::CliOpts;

    #[test]
    fn test_noop() {
        assert_eq!(None, CliOpts::parse(""));
    }

    #[test]
    fn test_parse_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::None),
            CliOpts::parse("help").unwrap()
        );
    }

    #[test]
    fn test_parse_exit() {
        assert_eq!(
            CliOpts::Command(Routes::Exit(false)),
            CliOpts::parse("exit").unwrap()
        );
    }

    #[test]
    fn test_parse_exit_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::Exit),
            CliOpts::parse("exit --help").unwrap()
        );
    }

    #[test]
    fn test_parse_exit_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Exit),
            CliOpts::parse("exit bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_exit_noprompt() {
        assert_eq!(
            CliOpts::Command(Routes::Exit(true)),
            CliOpts::parse("exit --noprompt").unwrap()
        );
    }

    #[test]
    fn test_parse_config_get_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::ConfigGet),
            CliOpts::parse("config-get --help").unwrap()
        );
    }

    #[test]
    fn test_parse_config_get_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::ConfigGet),
            CliOpts::parse("config-get bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_config_get_too_much() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::ConfigGet),
            CliOpts::parse("config-get input output").unwrap()
        );
    }

    #[test]
    fn test_parse_config_get_list() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigGet(None)),
            CliOpts::parse("config-get list").unwrap()
        );
    }
    #[test]
    fn test_parse_config_get_input() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigGet(Some(ConfigSetting::Input))),
            CliOpts::parse("config-get input").unwrap()
        );
    }

    #[test]
    fn test_parse_config_get_output() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigGet(Some(ConfigSetting::Output))),
            CliOpts::parse("config-get output").unwrap()
        );
    }

    #[test]
    fn test_parse_config_get_eot_input() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigGet(Some(ConfigSetting::EndOfTextInput))),
            CliOpts::parse("config-get end-of-text-input").unwrap()
        );
    }

    #[test]
    fn test_parse_config_set_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::ConfigSet),
            CliOpts::parse("config-set --help").unwrap()
        );
    }

    #[test]
    fn test_parse_config_set_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::ConfigSet),
            CliOpts::parse("config-set bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_config_set_too_much() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::ConfigSet),
            CliOpts::parse("config-set input output").unwrap()
        );
    }

    #[test]
    fn test_parse_config_set_input() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigSet(ConfigSetting::Input)),
            CliOpts::parse("config-set input").unwrap()
        );
    }

    #[test]
    fn test_parse_config_set_output() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigSet(ConfigSetting::Output)),
            CliOpts::parse("config-set output").unwrap()
        );
    }

    #[test]
    fn test_parse_config_set_eot_input() {
        assert_eq!(
            CliOpts::Command(Routes::ConfigSet(ConfigSetting::EndOfTextInput)),
            CliOpts::parse("config-set end-of-text-input").unwrap()
        );
    }

    #[test]
    fn test_parse_save_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::Save),
            CliOpts::parse("save --help").unwrap()
        );
    }

    #[test]
    fn test_parse_save_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Save),
            CliOpts::parse("save bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_save() {
        assert_eq!(
            CliOpts::Command(Routes::Save),
            CliOpts::parse("save").unwrap()
        );
    }

    #[test]
    fn test_parse_load_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::Load),
            CliOpts::parse("load --help").unwrap()
        );
    }

    #[test]
    fn test_parse_load_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Load),
            CliOpts::parse("load bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_load() {
        assert_eq!(
            CliOpts::Command(Routes::Load(false)),
            CliOpts::parse("load").unwrap()
        );
    }

    #[test]
    fn test_parse_load_no_prompt() {
        assert_eq!(
            CliOpts::Command(Routes::Load(true)),
            CliOpts::parse("load --noprompt").unwrap()
        );
    }

    #[test]
    fn test_parse_unload_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::Unload),
            CliOpts::parse("unload --help").unwrap()
        );
    }

    #[test]
    fn test_parse_unload_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Unload),
            CliOpts::parse("unload bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_unload() {
        assert_eq!(
            CliOpts::Command(Routes::Unload(false)),
            CliOpts::parse("unload").unwrap()
        );
    }

    #[test]
    fn test_parse_unload_no_prompt() {
        assert_eq!(
            CliOpts::Command(Routes::Unload(true)),
            CliOpts::parse("unload --noprompt").unwrap()
        );
    }

    #[test]
    fn test_parse_list_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::List),
            CliOpts::parse("list --help").unwrap()
        );

        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::List),
            CliOpts::parse("ls --help").unwrap()
        );
    }

    #[test]
    fn test_parse_list_bogus() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::List),
            CliOpts::parse("list bogus").unwrap()
        );

        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::List),
            CliOpts::parse("ls bogus").unwrap()
        );
    }

    #[test]
    fn test_parse_list() {
        assert_eq!(
            CliOpts::Command(Routes::List),
            CliOpts::parse("list").unwrap()
        );

        assert_eq!(
            CliOpts::Command(Routes::List),
            CliOpts::parse("ls").unwrap()
        );
    }

    #[test]
    fn test_parse_chmenu_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::ChMenu),
            CliOpts::parse("chmenu --help").unwrap()
        );
    }

    #[test]
    fn test_parse_chmenu_not_enough() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::ChMenu),
            CliOpts::parse("chmenu").unwrap()
        );
    }

    #[test]
    fn test_parse_chmenu_down() {
        assert_eq!(
            CliOpts::Command(Routes::ChMenu(Navigation::Down("foo".to_owned()))),
            CliOpts::parse("chmenu foo").unwrap()
        );
    }

    #[test]
    fn test_parse_chmenu_up() {
        assert_eq!(
            CliOpts::Command(Routes::ChMenu(Navigation::Up)),
            CliOpts::parse("chmenu ..").unwrap()
        );
    }

    #[test]
    fn test_parse_update_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::Update),
            CliOpts::parse("update --help").unwrap()
        );
    }

    #[test]
    fn test_parse_update_not_enough() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Update),
            CliOpts::parse("update").unwrap()
        );
    }

    #[test]
    fn test_parse_update_too_much() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Update),
            CliOpts::parse("update foo bar").unwrap()
        );
    }

    #[test]
    fn test_parse_update() {
        assert_eq!(
            CliOpts::Command(Routes::Update("foo".to_owned())),
            CliOpts::parse("update foo").unwrap()
        );
    }

    #[test]
    fn test_parse_info_help() {
        assert_eq!(
            CliOpts::Help(NO_PARSE_ERROR, HelpType::Info),
            CliOpts::parse("info --help").unwrap()
        );
    }

    #[test]
    fn test_parse_info_not_enough() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Info),
            CliOpts::parse("info").unwrap()
        );
    }

    #[test]
    fn test_parse_info_too_much() {
        assert_eq!(
            CliOpts::Help(PARSE_ERROR, HelpType::Info),
            CliOpts::parse("info foo bar").unwrap()
        );
    }

    #[test]
    fn test_parse_info() {
        assert_eq!(
            CliOpts::Command(Routes::Info("foo".to_owned())),
            CliOpts::parse("info foo").unwrap()
        );
    }
}
