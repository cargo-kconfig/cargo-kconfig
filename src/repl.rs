/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use crate::cli::CliOpts;
use crate::cli::PARSE_ERROR;
use crate::client::controller::ContinueOrStop;
use crate::client::controller::Controller;
use std::io::stderr;
use std::io::stdout;
#[cfg(test)]
use std::io::ErrorKind;
use std::io::{self, stdin, Write};
use std::ops::Add;
#[cfg(test)]
use std::sync::mpsc::Receiver;
#[cfg(test)]
use std::sync::mpsc::Sender;
#[cfg(test)]
use std::time::Duration;

pub(crate) fn run<P, T>(prompt: &P, terminal: &T) -> Result<(), io::Error>
where
    P: Prompt,
    T: Terminal,
{
    let controller = Controller::new(
        |s| terminal.writeln(s).unwrap(),
        |s| terminal.ewriteln(s).unwrap(),
        || terminal.readln().unwrap(),
        || print_secondary_prompt(prompt, terminal).unwrap(),
    );
    loop {
        let menu_names = controller.active_menu_list();
        if !menu_names.is_empty() {
            print_prefixed(menu_names, prompt, terminal)?;
        }
        print_primary_prompt(prompt, terminal)?;
        let result = read_line(terminal)?;
        if let Some(cli_opts) = CliOpts::parse(&result) {
            match cli_opts {
                // If the controller returns, it will indicate whether to stop execution,
                // or to continue with the next input. In the case of stopping executing,
                // the loop is to be terminated.
                CliOpts::Command(client_opt) => match controller.route(client_opt) {
                    ContinueOrStop::Continue => (),
                    ContinueOrStop::Stop => break Ok(()),
                },
                CliOpts::Help(parse_error, help_type) => {
                    if parse_error == PARSE_ERROR {
                        println!("⛔ Could not parse instructed command");
                    }
                    println!("{help_type}");
                }
            }
        }
    }
}

/// Prints the primary prompt
fn print_primary_prompt<P, T>(prompt: &P, terminal: &T) -> Result<(), io::Error>
where
    P: Prompt,
    T: Terminal,
{
    let primary = prompt.primary();
    terminal.write(&primary)?;
    terminal.flush()
}

/// Prints the secondary prompt
fn print_secondary_prompt<P, T>(prompt: &P, terminal: &T) -> Result<(), io::Error>
where
    P: Prompt,
    T: Terminal,
{
    let secondary = prompt.secondary();
    terminal.write(&secondary)?;
    terminal.flush()
}

/// Prints the menu names with breadcrumbs as specified in the prefix
fn print_prefixed<P, T>(menu_names: Vec<String>, prompt: &P, terminal: &T) -> Result<(), io::Error>
where
    P: Prompt,
    T: Terminal,
{
    let prefixed = prompt.prefix(menu_names.iter().map(|s| s as &str).collect());
    if !prefixed.is_empty() {
        terminal.write("[")?;
        terminal.write(&prefixed)?;
        terminal.writeln("]")?;
        terminal.flush()
    } else {
        Ok(())
    }
}

/// Reads input from the REPL
pub fn read_line<T>(terminal: &T) -> Result<String, io::Error>
where
    T: Terminal,
{
    terminal.readln()
}

/// The prompt allows the Read-Eval-Print-Loop to request to kinds of
/// prompts. The primary prompt, used for starting a new entry, and a
/// secondary prompt, used to continuing an existing entry.
pub trait Prompt {
    /// Displays the menu names
    fn prefix(&self, menu_names: Vec<&str>) -> String;

    /// Displays the primary prompt
    fn primary(&self) -> String;

    /// Displays the secondary prompt
    fn secondary(&self) -> String;
}

/// Allows for a basic prompt, implementing the Prompt trait
#[derive(Clone, Debug)]
pub struct BasicPrompt {
    primary: String,
    secondary: String,
    breadcrumb: String,
}

impl BasicPrompt {
    /// Creates a new basic prompt, with default characteristics, using
    /// ">>> " as primary prompt, "... " as secondary prompt and using
    /// " > " as breadcrumb separator for the prompt prefix, which in itself
    /// is printed with a newline character at the end.
    pub fn new() -> Self {
        Self {
            primary: ">>> ".to_owned(),
            secondary: "... ".to_owned(),
            breadcrumb: " > ".to_owned(),
        }
    }

    /// Use a different primary prompt
    pub fn with_primary(&self, s: &str) -> Self {
        let mut new_prompt = self.clone();
        new_prompt.primary = s.to_string();
        new_prompt
    }

    /// Use a different secondary prompt
    pub fn with_secondary(&self, s: &str) -> Self {
        let mut new_prompt = self.clone();
        new_prompt.secondary = s.to_string();
        new_prompt
    }

    /// Use a different breadcrumb separator for the menus
    pub fn with_breadcrumb(&self, s: &str) -> Self {
        let mut new_prompt = self.clone();
        new_prompt.breadcrumb = s.to_string();
        new_prompt
    }
}

/// Creates a basic prompt, defaulting with a primary prompt reading ">>> ",
/// a secondary prompt reading "... ". The prefix is determined
/// from the menu names, separated by breadcrumbs defaulting to " > ".

impl Prompt for BasicPrompt {
    fn prefix(&self, menu_names: Vec<&str>) -> std::string::String {
        let mut result = String::new();
        let mut is_empty = true;
        for menu_name in &menu_names {
            if !is_empty {
                result = result.add(&self.breadcrumb);
            } else {
                is_empty = false;
            }
            result = result.add(menu_name);
        }
        result
    }
    fn primary(&self) -> std::string::String {
        self.primary.clone()
    }
    fn secondary(&self) -> std::string::String {
        self.secondary.clone()
    }
}

/// This is a prompt which does not show input options, so it is easier to
/// read from an automated perspective during testing.
#[cfg(test)]
pub struct NoPrompt {}

#[cfg(test)]
impl Prompt for NoPrompt {
    fn prefix(&self, _: Vec<&str>) -> std::string::String {
        String::new()
    }
    fn primary(&self) -> std::string::String {
        String::new()
    }
    fn secondary(&self) -> std::string::String {
        String::new()
    }
}

/// Together with the Prompt trait, the IOManager deals with the IO of the CLI,
/// it is responsible for reading and writing data from an input respectively
/// to an output.
pub trait Terminal {
    /// Writes a string and ends it with a new line to the terminal
    fn writeln(&self, s: &str) -> Result<(), io::Error>;

    /// Writes a string to the terminal
    fn write(&self, s: &str) -> Result<(), io::Error>;

    /// Flushes the terminal
    fn flush(&self) -> Result<(), io::Error>;

    /// Writes a string and ends it with a new line to the error stream of the terminal
    fn ewriteln(&self, s: &str) -> Result<(), io::Error>;

    /// Writes a string to the error stream of the terminal
    fn ewrite(&self, s: &str) -> Result<(), io::Error>;

    /// Flushes the error stream of the terminal
    fn eflush(&self) -> Result<(), io::Error>;

    /// Reads a line of characters from the terminal
    fn readln(&self) -> Result<String, io::Error>;
}

/// This is the default implementation of the terminal trait, using
/// stdin() and stdout() functions of the std::io module.
pub struct Console {}

impl Console {
    /// Creates a new console instance, being a simple wrapper to
    /// the std() and stdout() functions.
    pub fn new() -> Self {
        Self {}
    }
}

impl Terminal for Console {
    fn writeln(&self, s: &str) -> Result<(), io::Error> {
        self.write(s)?;
        self.write("\n")
    }

    fn write(&self, s: &str) -> Result<(), io::Error> {
        stdout().write(s.to_string().as_bytes())?;
        Ok(())
    }

    fn flush(&self) -> Result<(), io::Error> {
        stdout().flush()
    }

    fn ewriteln(&self, s: &str) -> Result<(), io::Error> {
        self.ewrite(s)?;
        self.ewrite("\n")
    }

    fn ewrite(&self, s: &str) -> Result<(), io::Error> {
        stderr().write(s.to_string().as_bytes())?;
        Ok(())
    }

    fn eflush(&self) -> Result<(), io::Error> {
        stderr().flush()
    }

    fn readln(&self) -> Result<String, io::Error> {
        let mut buf = String::new();
        stdin().read_line(&mut buf)?;
        Ok(buf)
    }
}

#[cfg(test)]
pub struct TestTerminal {
    input: Receiver<char>,
    output: Sender<char>,
    eoutput: Sender<char>,
}
#[cfg(test)]

pub struct TestInterface {
    input: Sender<char>,
    output: Receiver<char>,
    eoutput: Receiver<char>,
}

#[cfg(test)]
pub fn test_repl() -> (TestTerminal, TestInterface) {
    use std::sync::mpsc;

    let (input_sender, input_receiver) = mpsc::channel::<char>();
    let (output_sender, output_receiver) = mpsc::channel::<char>();
    let (eoutput_sender, eoutput_receiver) = mpsc::channel::<char>();
    (
        TestTerminal {
            input: input_receiver,
            output: output_sender,
            eoutput: eoutput_sender,
        },
        TestInterface {
            input: input_sender,
            output: output_receiver,
            eoutput: eoutput_receiver,
        },
    )
}

#[cfg(test)]
impl Terminal for TestTerminal {
    fn writeln(&self, s: &str) -> Result<(), io::Error> {
        self.write(s)?;
        self.write("\n")
    }

    fn write(&self, s: &str) -> Result<(), io::Error> {
        for c in s.chars() {
            if let Err(e) = self.output.send(c) {
                return Err(io::Error::new(ErrorKind::Other, e));
            }
        }
        Ok(())
    }

    fn flush(&self) -> Result<(), io::Error> {
        Ok(())
    }

    fn ewriteln(&self, s: &str) -> Result<(), io::Error> {
        self.ewrite(s)?;
        self.ewrite("\n")
    }

    fn ewrite(&self, s: &str) -> Result<(), io::Error> {
        for c in s.chars() {
            if let Err(e) = self.eoutput.send(c) {
                return Err(io::Error::new(ErrorKind::Other, e));
            }
        }
        Ok(())
    }

    fn eflush(&self) -> Result<(), io::Error> {
        Ok(())
    }

    fn readln(&self) -> Result<String, io::Error> {
        let mut buf = String::new();
        Ok(loop {
            match self.input.recv_timeout(Duration::new(10, 0)) {
                Ok(c) => match c {
                    '\n' => break buf,
                    _ => buf.push(c),
                },
                Err(e) => return Err(io::Error::new(ErrorKind::Other, e)),
            }
        })
    }
}

#[cfg(test)]
impl TestInterface {
    pub fn writeln(&self, s: &str) -> Result<(), io::Error> {
        self.write(s)?;
        self.write("\n")
    }

    pub fn write(&self, s: &str) -> Result<(), io::Error> {
        for c in s.chars() {
            if let Err(e) = self.input.send(c) {
                return Err(io::Error::new(ErrorKind::Other, e));
            }
        }
        Ok(())
    }

    pub fn readln(&self) -> Result<String, io::Error> {
        let mut buf = String::new();
        Ok(loop {
            match self.output.recv_timeout(Duration::new(10, 0)) {
                Ok(c) => match c {
                    '\n' => break buf,
                    _ => buf.push(c),
                },
                Err(e) => return Err(io::Error::new(ErrorKind::Other, e)),
            }
        })
    }

    pub fn ereadln(&self) -> Result<String, io::Error> {
        let mut buf = String::new();
        Ok(loop {
            match self.eoutput.recv_timeout(Duration::new(10, 0)) {
                Ok(c) => match c {
                    '\n' => break buf,
                    _ => buf.push(c),
                },
                Err(e) => return Err(io::Error::new(ErrorKind::Other, e)),
            }
        })
    }
}
