/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This file contains utility functions to help the rest of the
//! application.

use std::{
    env,
    fs::File,
    path::{Path, PathBuf},
};

use kconfig_parser::{lex::source_lexer::Error, Lexer, SourceLexer};
use kconfig_represent::{ConfigRegistry, LoadError};

pub fn determine_file_name(s: &str) -> String {
    let dir = PathBuf::from(s);
    let abs = match dir.is_absolute() {
        true => dir,
        false => current_dir().join(dir),
    };
    format!(
        "{}",
        abs.into_os_string()
            .into_string()
            .unwrap_or_else(|_| panic!("Could not decode directory"))
    )
}

/// Simplified version to get the current directory, if there is
/// no current directory, hardcode it to "/", which would be weird,
/// but could be technically true.
pub fn current_dir() -> PathBuf {
    match std::env::current_dir() {
        Ok(p) => p,
        _ => PathBuf::from("/"),
    }
}

/// This function allows for reading a file, and pass it into a lexer.
/// If it fails, it will return with an error line.
pub(crate) fn lex_from_file(s: &str) -> Result<SourceLexer<Lexer<File>>, Error> {
    SourceLexer::new(
        &determine_file_name(s),
        |s| match File::open(determine_file_name(s)) {
            Ok(f) => Ok(Lexer::create(f)),
            _ => Err(format!("⛔ Can not read file {}", s)),
        },
        determine_file_name,
    )
}

/// This function loads .config file into the existing configuration registry
/// This code is not dead at all, but somehow rust triggers an erroneous warning
#[allow(dead_code)]
pub(crate) fn load_dot_config_into_registry(
    path: &str,
    registry: &mut ConfigRegistry,
) -> Option<LoadError> {
    if Path::new(&path).exists() {
        match registry.read_dotconfig_file(&path) {
            Err(e) => Some(e),
            Ok(_) => None,
        }
    } else {
        None
    }
}

/// Converts a canonical path to a relative path, if the given path is
/// a subpath of the relative path of the current working directory.
pub fn canonical_to_relative(filename: &str) -> String {
    match env::current_dir() {
        Ok(cwd) => match Path::new(filename).strip_prefix(cwd) {
            Ok(postfix) => match postfix.to_str() {
                Some(s) => s.to_string(),
                None => filename.to_string(),
            },
            Err(_) => filename.to_string(),
        },
        Err(_) => filename.to_string(),
    }
}
