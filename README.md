# Cargo Kconfig

The crate cargo-kconfig is both an extension on cargo as well as a set of macros
allowing for an application to use Kconfig configuration files.

The cargo-kconfig crate contains the cargo-kconfig command (when installed
through Cargo, this will also be usable as the command sequence "cargo
kconfig"), which implements a basic Kconfig menu user interface.

When the cargo-kconfig crate is used by another crate it contains several
Kconfig related macros, allowing for reading the Kconfig and associated .config
files, along with macros enabling reading out the values.

Currently, the package is under heavy development, any suggestions to enhance
the package are useful.
